const port = require('./port');
const saltRounds = require('./salt-rounds');
const sequelize = require('./sequelize');

module.exports = {
  port,
  saltRounds,
  sequelize,
};
