const port = process.env.NODE_ENV === 'test' ? 8101 : 8100;

module.exports = port;
