const Sequelize = require('sequelize');

const { sequelize } = require('../config');

const User = sequelize.define('user', {
  username: {
    type: Sequelize.CHAR(20),
    allowNull: false,
    unique: true,
  },
  email: {
    type: Sequelize.CHAR(30),
    allowNull: false,
    unique: true,
    validate: {
      isEmail: true,
    },
  },
  password: {
    type: Sequelize.CHAR(60),
    allowNull: false,
  },
});

module.exports = User;
