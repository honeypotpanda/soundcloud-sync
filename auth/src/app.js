require('dotenv').config();
const bodyParser = require('body-parser');
const express = require('express');
const logger = require('morgan');
const cors = require('cors');

const { port } = require('./config');
const { User } = require('./models');
const routes = require('./routes');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors({
  origin: 'http://localhost:8000',
}));
app.use('/auth', routes);

if (process.env.NODE_ENV !== 'test') {
  app.use(logger('dev'));
  User
    .sync()
    .then(() => console.log(`Connected to ${process.env.DB_DATABASE}_development.`))
    .catch(err => console.error(err));
}

app.listen(port, () => {
  if (process.env.NODE_ENV !== 'test') {
    console.log(`Listening on port ${port}.`);
  }
});

module.exports = app;
