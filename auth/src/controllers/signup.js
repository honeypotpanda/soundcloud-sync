const bcrypt = require('bcrypt');

const { User } = require('../models');
const { saltRounds } = require('../config');
const generateJwt = require('./generate-jwt');

const signup = (req, res) => {
  const { username, email, password, passwordConfirm } = req.body;

  if (!username) {
    return res.status(400).send({ message: 'Username is required.' });
  }

  if (!email) {
    return res.status(400).send({ message: 'Email is required.' });
  }

  if (!password) {
    return res.status(400).send({ message: 'Password is required.' });
  }

  if (!passwordConfirm) {
    return res.status(400).send({ message: 'Password confirm is required.' });
  }

  if (password !== passwordConfirm) {
    return res.status(400).send({ message: 'Passwords do not match.' });
  }

  bcrypt.hash(password, saltRounds, (err, hash) => {
    if (err) {
      return res.status(500).send(err);
    }

    User
      .create({ username, email, password: hash })
      .then((user) => {
        const token = generateJwt(user.get('id'));
        return res.status(200).send({ soundcloud_sync_token: token });
      })
      .catch((err) => {
        const firstError = err.errors[0];
        if (firstError.type === 'unique violation') {
          if (firstError.path === 'email') {
            return res.status(400).send({ message: 'Email has already been registered.' });
          } else if (firstError.path === 'username') {
            return res.status(400).send({ message: 'Username has already been registered.' });
          }
        }
        return res.status(400).send(firstError);
      });
  });
};

module.exports = signup;
