const jwt = require('jsonwebtoken');

const generateJwt = (id, expiry = '2h') => (
  jwt.sign({
    iss: 'soundcloud-sync',
    sub: id,
  }, process.env.JWT_SECRET, { expiresIn: expiry })
);

module.exports = generateJwt;
