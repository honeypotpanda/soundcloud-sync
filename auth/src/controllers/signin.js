const { compare } = require('bcrypt');

const { User } = require('../models');
const generateJwt = require('./generate-jwt');

const signin = (req, res) => {
  const { username, password } = req.body;

  if (!username) {
    return res.status(400).send({ message: 'Username is required.' });
  }

  if (!password) {
    return res.status(400).send({ message: 'Password is required.' });
  }

  User
    .findOne({
      where: {
        username,
      },
    })
    .then((user) => {
      compare(password, user.get('password'), (err, match) => {
        if (err) {
          return res.status(500).send(err);
        }

        if (match) {
          const token = generateJwt(user.get('id'));
          res.status(200).send({ soundcloud_sync_token: token });
        } else {
          res.status(400).send({ message: 'Your username or password was incorrect. Please try again.' });
        }
      });
    })
    .catch(() => {
      res.status(400).send({ message: 'Your username or password was incorrect. Please try again.' });
    });
};

module.exports = signin;
