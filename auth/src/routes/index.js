const express = require('express');

const controller = require('../controllers');

const router = express.Router();

router.post('/signin', controller.signin);
router.post('/signup', controller.signup);

module.exports = router;
