/* eslint no-unused-expressions: "off" */
/* global describe before after it */

const { hashSync } = require('bcrypt');
const chai = require('chai');
const chaiHttp = require('chai-http');

const app = require('../../src/app');
const { saltRounds } = require('../../src/config');
const { User } = require('../../src/models');

const expect = chai.expect;
const body = {
  username: 'test',
  password: 'test',
};
const hash = hashSync(body.password, saltRounds);
const user = {
  username: 'test',
  email: 'test@test.com',
  password: hash,
};

chai.use(chaiHttp);

describe('POST /auth/signin', () => {
  before((done) => {
    User
      .sync({ force: true })
      .then(() => User.create(user))
      .then(() => done())
      .catch(done);
  });

  after((done) => {
    User
      .drop()
      .then(() => done())
      .catch(done);
  });

  it('should sign user in with valid credentials', (done) => {
    chai
      .request(app)
      .post('/auth/signin')
      .send(body)
      .end((err, res) => {
        expect(err).to.not.exist;
        expect(res.status).to.equal(200);
        expect(res.body.soundcloud_sync_token).to.exist;
        done();
      });
  });

  it('should not sign user in without a username', (done) => {
    chai
      .request(app)
      .post('/auth/signin')
      .send(Object.assign({}, body, { username: '' }))
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(400);
        expect(res.body.message).to.equal('Username is required.');
        done();
      });
  });

  it('should not sign user in without a password', (done) => {
    chai
      .request(app)
      .post('/auth/signin')
      .send(Object.assign({}, body, { password: '' }))
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(400);
        expect(res.body.message).to.equal('Password is required.');
        done();
      });
  });

  it('should not sign user in with a non-registered username', (done) => {
    chai
      .request(app)
      .post('/auth/signin')
      .send(Object.assign({}, body, { username: 'non-registered-username' }))
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(400);
        expect(res.body.message).to.equal('Your username or password was incorrect. Please try again.');
        done();
      });
  });

  it('should not sign user in with an incorrect password', (done) => {
    chai
      .request(app)
      .post('/auth/signin')
      .send(Object.assign({}, body, { password: 'incorrect-password' }))
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(400);
        expect(res.body.message).to.equal('Your username or password was incorrect. Please try again.');
        done();
      });
  });
});
