/* eslint no-unused-expressions: "off" */
/* global describe beforeEach afterEach it */

const chai = require('chai');
const chaiHttp = require('chai-http');

const app = require('../../src/app');
const { User } = require('../../src/models');

const expect = chai.expect;
const body = {
  username: 'test',
  email: 'test@test.com',
  password: 'test',
  passwordConfirm: 'test',
};

chai.use(chaiHttp);

describe('POST /auth/signup', () => {
  beforeEach((done) => {
    User
      .sync({ force: true })
      .then(() => done())
      .catch(done);
  });

  afterEach((done) => {
    User
      .drop()
      .then(() => done())
      .catch(done);
  });

  it('should create a new user', (done) => {
    chai
      .request(app)
      .post('/auth/signup')
      .send(body)
      .end((err, res) => {
        expect(err).to.not.exist;
        expect(res.status).to.equal(200);
        expect(res.body.soundcloud_sync_token).to.exist;
        done();
      });
  });

  it('should not create a new user without a username', (done) => {
    chai
      .request(app)
      .post('/auth/signup')
      .send(Object.assign({}, body, { username: '' }))
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(400);
        expect(res.body.message).to.equal('Username is required.');
        done();
      });
  });

  it('should not create a new user without an email', (done) => {
    chai
      .request(app)
      .post('/auth/signup')
      .send(Object.assign({}, body, { email: '' }))
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(400);
        expect(res.body.message).to.equal('Email is required.');
        done();
      });
  });

  it('should not create a new user without a password', (done) => {
    chai
      .request(app)
      .post('/auth/signup')
      .send(Object.assign({}, body, { password: '' }))
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(400);
        expect(res.body.message).to.equal('Password is required.');
        done();
      });
  });

  it('should not create a new user without a password confirm', (done) => {
    chai
      .request(app)
      .post('/auth/signup')
      .send(Object.assign({}, body, { passwordConfirm: '' }))
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(400);
        expect(res.body.message).to.equal('Password confirm is required.');
        done();
      });
  });

  it('should not create a new user if password and password confirm do not match', (done) => {
    chai
      .request(app)
      .post('/auth/signup')
      .send(Object.assign({}, body, { passwordConfirm: 'incorrect password' }))
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(400);
        expect(res.body.message).to.equal('Passwords do not match.');
        done();
      });
  });

  it('should not create a new user with a registered email', (done) => {
    User
      .create(body)
      .then(() => {
        chai
          .request(app)
          .post('/auth/signup')
          .send(Object.assign({}, body, { username: 'test2' }))
          .end((err, res) => {
            expect(err).to.exist;
            expect(res.status).to.equal(400);
            expect(res.body.message).to.equal('Email has already been registered.');
            done();
          });
      })
      .catch(done);
  });

  it('should not create a new user with a registered username', (done) => {
    User
      .create(body)
      .then(() => {
        chai
          .request(app)
          .post('/auth/signup')
          .send(Object.assign({}, body, { email: 'test2@test.com' }))
          .end((err, res) => {
            expect(err).to.exist;
            expect(res.status).to.equal(400);
            expect(res.body.message).to.equal('Username has already been registered.');
            done();
          });
      })
      .catch(done);
  });
});
