const Sequelize = require('sequelize');

const host = process.env.DB_HOST;
const database = process.env.NODE_ENV === 'test'
  ? `${process.env.DB_DATABASE}_test`
  : `${process.env.DB_DATABASE}_development`;
const user = process.env.DB_USER;
const password = process.env.DB_PASSWORD;

module.exports = new Sequelize(database, user, password, {
  host,
  dialect: 'mysql',
  logging: false,
  timezone: 'America/Los_Angeles',
});
