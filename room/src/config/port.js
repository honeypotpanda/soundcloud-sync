const port = process.env.NODE_ENV === 'test' ? 8201 : 8200;

module.exports = port;
