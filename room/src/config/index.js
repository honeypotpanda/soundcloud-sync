const port = require('./port');
const sequelize = require('./sequelize');

module.exports = {
  port,
  sequelize,
};
