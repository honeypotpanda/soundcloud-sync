const express = require('express');
const jwt = require('express-jwt');

const controller = require('../controllers');

const router = express.Router();

router.get('/rooms', controller.getRooms);
router.post('/rooms', jwt({ secret: process.env.JWT_SECRET }), controller.createRoom);
router.get('/rooms/:name', controller.getRoomInfo);

module.exports = router;
