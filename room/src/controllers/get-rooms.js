const { Room } = require('../models');

const getRooms = (req, res) => {
  Room
    .findAll({
      attributes: ['name'],
    })
    .then(rooms => res.status(200).send({ rooms }))
    .catch(err => res.status(400).send(err));
};

module.exports = getRooms;
