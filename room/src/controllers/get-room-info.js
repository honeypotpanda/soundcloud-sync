const redis = require('redis');

const { Room } = require('../models');

const port = process.env.NODE_ENV === 'test' ? 7000 : 6379;
const client = redis.createClient({ port });
const batch = client.batch();

const getRoomInfo = (req, res) => {
  const name = req.params.name;

  Room
    .findOne({
      where: {
        name,
      },
    })
    .then((room) => {
      if (!room) {
        return res.status(404).send({ message: 'Room does not exist.' });
      }

      batch.lrange(`soundcloud-sync:room:${name}:tracks`, 0, -1);
      batch.get(`soundcloud-sync:room:${name}:initial-time`);
      batch.get(`soundcloud-sync:room:${name}:start-time`);
      batch.get(`soundcloud-sync:room:${name}:is-playing`);
      batch.get(`soundcloud-sync:room:${name}:track-index`);
      batch.exec((err, [tracks, initialTime, startTime, isPlaying, trackIndex]) => {
        if (err) {
          return res.status(400).send(err);
        }
        return res.status(200).send({
          tracks: tracks.map(track => JSON.parse(track)),
          initialTime: parseInt(initialTime, 10),
          startTime: parseInt(startTime, 10),
          isPlaying: isPlaying === '1',
          trackIndex: parseInt(trackIndex, 10),
        });
      });
    })
    .catch((err) => {
      res.status(400).send(err);
    });
};

module.exports = getRoomInfo;
