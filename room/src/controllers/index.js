const createRoom = require('./create-room');
const getRoomInfo = require('./get-room-info');
const getRooms = require('./get-rooms');

module.exports = {
  createRoom,
  getRoomInfo,
  getRooms,
};
