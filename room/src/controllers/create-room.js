const redis = require('redis');

const { Room } = require('../models');

const port = process.env.NODE_ENV === 'test' ? 7000 : 6379;
const client = redis.createClient({ port });
const batch = client.batch();

const createRoom = (req, res) => {
  const { name } = req.body;

  if (!name) {
    return res.status(400).send({ message: 'Name is required.' });
  }

  Room
    .create({ name })
    .then(() => {
      batch.del(`soundcloud-sync:room:${name}:tracks`);
      batch.set(`soundcloud-sync:room:${name}:initial-time`, 0);
      batch.set(`soundcloud-sync:room:${name}:start-time`, 0);
      batch.set(`soundcloud-sync:room:${name}:is-playing`, 0);
      batch.set(`soundcloud-sync:room:${name}:track-index`, 0);
      batch.exec((err) => {
        if (err) {
          return res.status(400).send({ message: 'Unable to initialize room.' });
        }
        res.status(200).send({ message: 'Successfully created room.' });
      });
    })
    .catch((err) => {
      const firstError = err.errors[0];
      if (firstError.type === 'unique violation') {
        if (firstError.path === 'name') {
          return res.status(400).send({ message: 'Room has already been registered.' });
        }
      }
      return res.status(400).send(firstError);
    });
};

module.exports = createRoom;
