const Sequelize = require('sequelize');

const { sequelize } = require('../config');

const Room = sequelize.define('room', {
  name: {
    type: Sequelize.CHAR(20),
    allowNull: false,
    unique: true,
  },
});

module.exports = Room;
