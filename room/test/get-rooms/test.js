/* eslint no-unused-expressions: "off" */
/* global describe before after it */

const chai = require('chai');
const chaiHttp = require('chai-http');

const app = require('../../src/app');
const { Room } = require('../../src/models');

const expect = chai.expect;

chai.use(chaiHttp);

describe('GET /api/rooms', () => {
  before((done) => {
    Room
      .sync({ force: true })
      .then(() => Room.create({ name: 'test1' }))
      .then(() => Room.create({ name: 'test2' }))
      .then(() => Room.create({ name: 'test3' }))
      .then(() => done())
      .catch(done);
  });

  after((done) => {
    Room
      .drop()
      .then(() => done())
      .catch(done);
  });

  it('should get rooms', (done) => {
    chai
      .request(app)
      .get('/api/rooms')
      .end((err, res) => {
        expect(err).to.not.exist;
        expect(res.status).to.equal(200);
        expect(res.body.rooms).to.deep.equal([
          { name: 'test1' },
          { name: 'test2' },
          { name: 'test3' },
        ]);
        done();
      });
  });
});
