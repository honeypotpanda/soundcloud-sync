/* eslint no-unused-expressions: "off" */
/* global describe before after it */

const chai = require('chai');
const chaiHttp = require('chai-http');
const jwt = require('jsonwebtoken');
const redis = require('redis');
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const app = require('../../src/app');
const { Room } = require('../../src/models');

const expect = chai.expect;
const client = redis.createClient({ port: 7000 });
const multi = client.multi();
const room = 'test';
const body = {
  name: 'test',
};
const generateJwt = (id, expiry, secret) => (
  jwt.sign({
    iss: 'soundcloud_sync',
    sub: id,
  }, secret, { expiresIn: expiry })
);
const token = generateJwt(1, '2h', process.env.JWT_SECRET);

chai.use(chaiHttp);

describe('GET /api/rooms/:name', () => {
  before((done) => {
    Room
      .sync({ force: true })
      .then(() => {
        client.flushall((err) => {
          if (err) {
            done(err);
          }
          done();
        });
      })
      .catch(done);
  });

  after((done) => {
    Room
      .drop()
      .then(() => {
        client.flushall((err) => {
          if (err) {
            done(err);
          }
          done();
        });
      })
      .catch(done);
  });

  it('should get room info', (done) => {
    const track1 = {
      id: '1',
      html: 'test1',
      title: 'test1',
      username: 'test1',
      artwork_url: 'http://www.test1.com',
      permalink_url: 'http://www.test1.com',
    };
    const track2 = {
      id: '2',
      html: 'test2',
      title: 'test2',
      username: 'test2',
      artwork_url: 'http://www.test2.com',
      permalink_url: 'http://www.test2.com',
    };
    chai
      .request(app)
      .post('/api/rooms')
      .set('Authorization', `Bearer ${token}`)
      .send(body)
      .end((err, res) => {
        expect(err).to.not.exist;
        expect(res.status).to.equal(200);
        expect(res.body.message).to.equal('Successfully created room.');
        multi.rpush(`soundcloud-sync:room:${room}:tracks`, JSON.stringify(track1));
        multi.rpush(`soundcloud-sync:room:${room}:tracks`, JSON.stringify(track2));
        multi.execAsync().then(() => {
          chai
            .request(app)
            .get('/api/rooms/test')
            .end((err, res) => {
              expect(err).to.not.exist;
              expect(res.status).to.equal(200);
              expect(res.body.tracks).to.deep.equal([track1, track2]);
              expect(res.body.initialTime).to.deep.equal(0);
              expect(res.body.startTime).to.deep.equal(0);
              expect(res.body.isPlaying).to.deep.equal(false);
              expect(res.body.trackIndex).to.deep.equal(0);
              done();
            });
        }).catch(err => done(err));
      });
  });

  it('should not get room info for a room that has not been created', (done) => {
    chai
      .request(app)
      .get('/api/rooms/test2')
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(404);
        expect(res.body.message).to.equal('Room does not exist.');
        done();
      });
  });
});
