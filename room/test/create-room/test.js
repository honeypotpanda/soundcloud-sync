/* eslint no-unused-expressions: "off" */
/* global describe beforeEach afterEach it */

const chai = require('chai');
const chaiHttp = require('chai-http');
const jwt = require('jsonwebtoken');
const redis = require('redis');

const app = require('../../src/app');
const { Room } = require('../../src/models');

const expect = chai.expect;
const body = {
  name: 'test',
};
const generateJwt = (id, expiry, secret) => (
  jwt.sign({
    iss: 'soundcloud_sync',
    sub: id,
  }, secret, { expiresIn: expiry })
);
const validToken = generateJwt(1, '2h', process.env.JWT_SECRET);
const invalidToken = generateJwt(1, '2h', 'invalid secret');
const client = redis.createClient({ port: 7000 });
const batch = client.batch();

chai.use(chaiHttp);

describe('POST /api/rooms', () => {
  beforeEach((done) => {
    Room
      .sync({ force: true })
      .then(() => done())
      .catch(done);
  });

  afterEach((done) => {
    Room
      .drop()
      .then(() => {
        client.flushall((err) => {
          if (err) {
            return done(err);
          }
          done();
        });
      })
      .catch(done);
  });

  it('should create room with a unique room name', (done) => {
    chai
      .request(app)
      .post('/api/rooms')
      .set('Authorization', `Bearer ${validToken}`)
      .send(body)
      .end((err, res) => {
        expect(err).to.not.exist;
        expect(res.status).to.equal(200);
        expect(res.body.message).to.equal('Successfully created room.');
        done();
      });
  });

  it('should not create room without a room name', (done) => {
    chai
      .request(app)
      .post('/api/rooms')
      .set('Authorization', `Bearer ${validToken}`)
      .send()
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(400);
        expect(res.body.message).to.equal('Name is required.');
        done();
      });
  });

  it('should not create room with a room name that has already been registered', (done) => {
    chai
      .request(app)
      .post('/api/rooms')
      .set('Authorization', `Bearer ${validToken}`)
      .send(body)
      .end((err, res) => {
        expect(err).to.not.exist;
        expect(res.status).to.equal(200);
        expect(res.body.message).to.equal('Successfully created room.');
        chai
          .request(app)
          .post('/api/rooms')
          .set('Authorization', `Bearer ${validToken}`)
          .send(body)
          .end((err, res) => {
            expect(err).to.exist;
            expect(res.status).to.equal(400);
            expect(res.body.message).to.equal('Room has already been registered.');
            done();
          });
      });
  });

  it('should not create room without a token', (done) => {
    chai
      .request(app)
      .post('/api/rooms')
      .send()
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(401);
        done();
      });
  });

  it('should not create room with an invalid token', (done) => {
    chai
      .request(app)
      .post('/api/rooms')
      .set('Authorization', `Bearer ${invalidToken}`)
      .send()
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(401);
        done();
      });
  });

  it('should initialize room', (done) => {
    chai
      .request(app)
      .post('/api/rooms')
      .set('Authorization', `Bearer ${validToken}`)
      .send(body)
      .end((err, res) => {
        expect(err).to.not.exist;
        expect(res.status).to.equal(200);
        expect(res.body.message).to.equal('Successfully created room.');
        batch.lrange('soundcloud-sync:room:test:tracks', 0, -1);
        batch.get('soundcloud-sync:room:test:initial-time');
        batch.get('soundcloud-sync:room:test:start-time');
        batch.get('soundcloud-sync:room:test:is-playing');
        batch.get('soundcloud-sync:room:test:track-index');
        batch.exec((err, [tracks, initialTime, startTime, isPlaying, trackIndex]) => {
          expect(err).to.not.exist;
          expect(tracks).to.deep.equal([]);
          expect(initialTime).to.equal('0');
          expect(startTime).to.equal('0');
          expect(isPlaying).to.equal('0');
          expect(trackIndex).to.equal('0');
          done();
        });
      });
  });
});
