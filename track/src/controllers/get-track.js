const axios = require('axios');
const { escape } = require('lodash');
const { v4 } = require('uuid');

const getTrack = (req, res) => {
  const { url } = req.query;

  if (!url) {
    return res.status(400).send({ message: 'URL is missing.' });
  }

  if (!(url.startsWith('http://soundcloud.com') || url.startsWith('https://soundcloud.com'))) {
    return res.status(400).send({ message: 'Invalid URL.' });
  }

  axios.get('https://soundcloud.com/oembed', {
    params: {
      format: 'json',
      url: escape(url),
      maxheight: 200,
    },
  }).then(({ data }) => {
    const id = v4();
    res.status(200).send(Object.assign({}, data, { id }));
  }).catch(({ response }) => {
    res.status(response.status).send({ message: response.statusText });
  });
};

module.exports = getTrack;
