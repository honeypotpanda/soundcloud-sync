const addTrack = require('./add-track');
const deleteTrack = require('./delete-track');
const getTrack = require('./get-track');

module.exports = {
  addTrack,
  deleteTrack,
  getTrack,
};
