const redis = require('redis');
const { findIndex } = require('lodash');

const port = process.env.NODE_ENV === 'test' ? 7000 : 6379;
const client = redis.createClient({ port });

const deleteTrack = (req, res) => {
  const { room, id } = req.params;

  client.lrange(`soundcloud-sync:room:${room}:tracks`, 0, -1, (err, reply) => {
    if (err) {
      return res.status(400).send(err);
    }

    const tracksJSON = reply.map(track => JSON.parse(track));
    const index = findIndex(tracksJSON, { id });

    if (index === -1) {
      return res.status(404).send({ message: `Unable to find track ${id}.` });
    }

    const trackToBeDeleted = tracksJSON[index];
    client.lrem(`soundcloud-sync:room:${room}:tracks`, 1, JSON.stringify(trackToBeDeleted), (err) => {
      if (err) {
        return res.status(400).send(err);
      }

      return res.status(200).send({
        message: `Successfully deleted track ${id}.`,
        index,
      });
    });
  });
};

module.exports = deleteTrack;
