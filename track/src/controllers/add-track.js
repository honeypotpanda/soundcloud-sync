const axios = require('axios');
const { v4 } = require('uuid');
const redis = require('redis');

const port = process.env.NODE_ENV === 'test' ? 7000 : 6379;
const client = redis.createClient({ port });

const addTrack = (req, res) => {
  const { url, room } = req.body;

  if (!url) {
    return res.status(400).send({ message: 'URL is missing.' });
  }

  if (!(url.startsWith('http://soundcloud.com') || url.startsWith('https://soundcloud.com'))) {
    return res.status(400).send({ message: 'Invalid URL.' });
  }

  if (!room) {
    return res.status(400).send({ message: 'Room is missing.' });
  }

  axios.all([
    axios.get('https://soundcloud.com/oembed', {
      params: {
        format: 'json',
        url,
        maxheight: 200,
      },
    }),
    axios.get('http://api.soundcloud.com/resolve', {
      params: {
        url,
        client_id: process.env.SOUNDCLOUD_CLIENT_ID,
      },
    }),
  ]).then(axios.spread((oembed, resolve) => {
    const oembedData = oembed.data;
    const resolveData = resolve.data;
    const payload = {
      id: v4(),
      html: oembedData.html,
      title: resolveData.title,
      username: resolveData.user.username,
      artwork_url: resolveData.artwork_url,
      permalink_url: resolveData.permalink_url,
    };
    client.rpush(`soundcloud-sync:room:${room}:tracks`, JSON.stringify(payload), (err) => {
      if (err) {
        return res.status(400).send({ message: 'An error occurred when adding track.' });
      }
      return res.status(200).json(payload);
    });
  })).catch(({ response }) => {
    res.status(response.status).send({ message: response.statusText });
  });
};

module.exports = addTrack;
