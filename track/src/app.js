require('dotenv').config();
const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');
const logger = require('morgan');
const redis = require('redis');
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const routes = require('./routes');
const { port } = require('./config');

const redisPort = process.env.NODE_ENV === 'test' ? 7000 : 6379;
const client = redis.createClient({ port: redisPort });
const multi = client.multi();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors({
  origin: 'http://localhost:8000',
}));

app.use('/api', routes);

if (process.env.NODE_ENV !== 'test') {
  app.use(logger('dev'));
}

io.on('connection', (socket) => {
  if (process.env.NODE_ENV !== 'test') {
    console.log(`${socket.id} has connected.`);
  }

  socket.on('join room', ({ room }) => {
    socket.join(room);
  });

  socket.on('leave room', ({ room }) => {
    socket.leave(room);
  });

  socket.on('play', ({ time, room }) => {
    multi.set(`soundcloud-sync:room:${room}:initial-time`, time);
    multi.set(`soundcloud-sync:room:${room}:start-time`, Date.now());
    multi.set(`soundcloud-sync:room:${room}:is-playing`, 1);
    multi.execAsync().then(() => {
      socket.broadcast.to(room).emit('play', { time });
    }).catch((err) => {
      console.error(err);
    });
  });

  socket.on('pause', ({ room }) => {
    client.setAsync(`soundcloud-sync:room:${room}:is-playing`, 0).then(() => {
      socket.broadcast.to(room).emit('pause');
    }).catch((err) => {
      console.error(err);
    });
  });

  socket.on('finish', ({ nextTrackIndex, room }) => {
    multi.set(`soundcloud-sync:room:${room}:initial-time`, 0);
    multi.set(`soundcloud-sync:room:${room}:start-time`, Date.now());
    multi.set(`soundcloud-sync:room:${room}:is-playing`, 1);
    multi.set(`soundcloud-sync:room:${room}:track-index`, nextTrackIndex);
    multi.execAsync().then(() => {
      socket.broadcast.emit('set next track', { nextTrackIndex });
    }).catch(err => console.error(err));
  });

  socket.on('seek', ({ time, room }) => {
    multi.set(`soundcloud-sync:room:${room}:initial-time`, time);
    multi.set(`soundcloud-sync:room:${room}:start-time`, Date.now());
    multi.execAsync().then(() => {
      socket.broadcast.to(room).emit('seek', { time });
    }).catch(err => console.error(err));
  });

  socket.on('add track', ({ track, room }) => {
    socket.broadcast.to(room).emit('add track', track);
  });

  socket.on('remove track', ({ index, room }) => {
    socket.broadcast.to(room).emit('remove track', { index });
  });

  socket.on('disconnect', () => {
    if (process.env.NODE_ENV !== 'test') {
      console.log(`${socket.id} has disconnected.`);
    }
  });
});

server.listen(port, () => {
  if (process.env.NODE_ENV !== 'test') {
    console.log(`Listening on port ${port}.`);
  }
});

module.exports = server;
