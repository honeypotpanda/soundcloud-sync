const express = require('express');

const controller = require('../controllers');

const router = express.Router();

router.post('/tracks', controller.addTrack);
router.delete('/rooms/:room/tracks/:id', controller.deleteTrack);
router.get('/tracks', controller.getTrack);

module.exports = router;
