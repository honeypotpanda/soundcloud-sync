const port = process.env.NODE_ENV === 'test' ? 8301 : 8300;

module.exports = port;
