/* eslint no-unused-expressions: "off" */
/* global describe before after it */

const chai = require('chai');
const chaiHttp = require('chai-http');
const nock = require('nock');
const redis = require('redis');

const app = require('../../src/app');

const expect = chai.expect;
const client = redis.createClient({ port: 7000 });
const url = 'http://soundcloud.com/forss/flickermood';
const body = {
  room: 'test',
  url,
};

chai.use(chaiHttp);

describe('POST /api/tracks', () => {
  before((done) => {
    client.flushall((err) => {
      if (err) {
        done(err);
      }
      done();
    });
  });

  after((done) => {
    client.flushall((err) => {
      if (err) {
        done(err);
      }
      done();
    });
  });

  it('should add track', (done) => {
    nock('https://soundcloud.com')
      .get('/oembed')
      .query({
        format: 'json',
        url,
        maxheight: 200,
      })
      .reply(200, {
        version: 1,
        type: 'rich',
        provider_name: 'SoundCloud',
        provider_url: 'http://soundcloud.com',
        height: 400,
        width: '100%',
        title: 'Flickermood by Forss',
        description: 'From the Soulhack album,&nbsp;recently featured in this ad <a href="https://www.dswshoes.com/tv_commercial.jsp?m=october2007">https://www.dswshoes.com/tv_commercial.jsp?m=october2007</a> ',
        thumbnail_url: 'http://i1.sndcdn.com/artworks-000067273316-smsiqx-t500x500.jpg',
        html: '<iframe width="100%" height="400" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?visual=true&url=https%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F293&show_artwork=true"></iframe>',
        author_name: 'Forss',
        author_url: 'https://soundcloud.com/forss',
      });
    nock('http://api.soundcloud.com')
      .get('/resolve')
      .query({
        url,
        client_id: process.env.SOUNDCLOUD_CLIENT_ID,
      })
      .reply(200, {
        kind: 'track',
        id: 293,
        created_at: '2007/09/22 14:45:46 +0000',
        user_id: 183,
        duration: 213890,
        commentable: true,
        state: 'finished',
        original_content_size: 37723728,
        last_modified: '2017/01/12 18:06:40 +0000',
        sharing: 'public',
        tag_list: 'downtempo',
        permalink: 'flickermood',
        streamable: true,
        embeddable_by: 'all',
        purchase_url: 'http://sonarkollektiv.com/tracks/DE-P96-03-00042',
        purchase_title: null,
        label_id: 1771,
        genre: 'Electronic',
        title: 'Flickermood',
        description: 'From the Soulhack album, recently featured in this ad <a href="https://www.dswshoes.com/tv_commercial.jsp?m=october2007" rel="nofollow">https://www.dswshoes.com/tv_commercial.jsp?m=october2007</a> ',
        label_name: 'sonarkollektiv',
        release: 'SK006',
        track_type: 'original',
        key_signature: '',
        isrc: 'DEP960300042',
        video_url: 'http://vimeo.com/3302330',
        bpm: 74,
        release_year: 2003,
        release_month: 6,
        release_day: 2,
        original_format: 'aiff',
        license: 'all-rights-reserved',
        uri: 'https://api.soundcloud.com/tracks/293',
        user: {
          id: 183,
          kind: 'user',
          permalink: 'forss',
          username: 'Forss',
          last_modified: '2016/12/17 18:44:44 +0000',
          uri: 'https://api.soundcloud.com/users/183',
          permalink_url: 'http://soundcloud.com/forss',
          avatar_url: 'https://i1.sndcdn.com/avatars-000012778523-59quu3-large.jpg',
        },
        permalink_url: 'https://soundcloud.com/forss/flickermood',
        artwork_url: 'https://i1.sndcdn.com/artworks-000067273316-smsiqx-large.jpg',
        stream_url: 'https://api.soundcloud.com/tracks/293/stream',
        download_url: 'https://api.soundcloud.com/tracks/293/download',
        playback_count: 535834,
        download_count: 17111,
        favoritings_count: 2268,
        reposts_count: 281,
        comment_count: 455,
        label: {
          id: 1771,
          kind: 'user',
          permalink: 'sonar-kollektiv',
          username: 'Sonar Kollektiv',
          last_modified: '2017/01/30 11:58:43 +0000',
          uri: 'https://api.soundcloud.com/users/1771',
          permalink_url: 'http://soundcloud.com/sonar-kollektiv',
          avatar_url: 'https://i1.sndcdn.com/avatars-000197546520-gztpcl-large.jpg',
        },
        downloadable: true,
        waveform_url: 'https://w1.sndcdn.com/cWHNerOLlkUq_m.png',
        attachments_uri: 'https://api.soundcloud.com/tracks/293/attachments',
      });
    chai
      .request(app)
      .post('/api/tracks')
      .send(body)
      .end((err, res) => {
        expect(err).to.not.exist;
        expect(res.status).to.equal(200);
        expect(res.body.id).to.exist;
        expect(res.body.html).to.exist;
        expect(res.body.title).to.exist;
        expect(res.body.username).to.exist;
        expect(res.body.artwork_url).to.exist;
        expect(res.body.permalink_url).to.exist;
        client.lrange('soundcloud-sync:room:test:tracks', 0, -1, (err, reply) => {
          expect(err).to.not.exist;
          expect(reply).to.have.length(1);
          done();
        });
      });
  });

  it('should not add track without a SoundCloud URL', (done) => {
    chai
      .request(app)
      .post('/api/tracks')
      .send(Object.assign({}, body, { url: '' }))
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(400);
        expect(res.body.message).to.equal('URL is missing.');
        done();
      });
  });

  it('should not get track with an invalid SoundCloud URL', (done) => {
    chai
      .request(app)
      .post('/api/tracks')
      .send(Object.assign({}, body, { url: 'http://google.com' }))
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(400);
        expect(res.body.message).to.equal('Invalid URL.');
        done();
      });
  });

  it('should not get track that does not exist', (done) => {
    nock('https://soundcloud.com')
      .get('/oembed')
      .query({
        format: 'json',
        url,
        maxheight: 200,
      })
      .reply(404);
    nock('http://api.soundcloud.com')
      .get('/resolve')
      .query({
        url,
        client_id: process.env.SOUNDCLOUD_CLIENT_ID,
      })
      .reply(404);
    chai
      .request(app)
      .post('/api/tracks')
      .send(body)
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(404);
        expect(res.body).to.have.property('message');
        done();
      });
  });
});
