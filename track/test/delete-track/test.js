/* eslint no-unused-expressions: "off" */
/* global describe before after it */

const chai = require('chai');
const chaiHttp = require('chai-http');
const redis = require('redis');

const app = require('../../src/app');

const expect = chai.expect;
const client = redis.createClient({ port: 7000 });
const multi = client.multi();
const room = 'test';
const id = 'test1';
const track1 = {
  id: 'test1',
  html: 'test1',
};
const track2 = {
  id: 'test2',
  html: 'test2',
};

chai.use(chaiHttp);

describe('DELETE /api/rooms/:room/tracks/:id', () => {
  before((done) => {
    client.flushall((err) => {
      if (err) {
        done(err);
      }
      done();
    });
  });

  after((done) => {
    client.flushall((err) => {
      if (err) {
        done(err);
      }
      done();
    });
  });

  it('should delete track', (done) => {
    multi.rpush(`soundcloud-sync:room:${room}:tracks`, JSON.stringify(track1));
    multi.rpush(`soundcloud-sync:room:${room}:tracks`, JSON.stringify(track2));
    multi.exec((err, replies) => {
      expect(err).to.not.exist;
      expect(replies[0]).to.equal(1);
      expect(replies[1]).to.equal(2);
      chai
        .request(app)
        .delete(`/api/rooms/${room}/tracks/${id}`)
        .end((err, res) => {
          expect(err).to.not.exist;
          expect(res.status).to.equal(200);
          client.lrange(`soundcloud-sync:room:${room}:tracks`, 0, -1, (err, reply) => {
            expect(err).to.not.exist;
            expect(JSON.parse(reply)).to.deep.equal(track2);
            done();
          });
        });
    });
  });

  it('should 404 for a track that does not exist', (done) => {
    chai
      .request(app)
      .delete(`/api/rooms/${room}/tracks/3`)
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(404);
        expect(res.body.message).to.equal('Unable to find track 3.');
        done();
      });
  });
});
