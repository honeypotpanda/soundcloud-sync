/* eslint no-unused-expressions: "off" */
/* global describe beforeEach afterEach after it */

const chai = require('chai');
const redis = require('redis');
const bluebird = require('bluebird');
const io = require('socket.io-client');

require('../../src/app');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const expect = chai.expect;
const client = redis.createClient({ port: 7000 });
const multi = client.multi();
let sender = null;
let receiver = null;
const room = 'test';
const data = {
  nextTrackIndex: 0,
  room,
};

describe('finish event', () => {
  beforeEach((done) => {
    client.flushallAsync().then(() => {
      sender = io.connect('http://localhost:8301');
      receiver = io.connect('http://localhost:8301');
      sender.emit('join room', { room });
      receiver.emit('join room', { room });
      multi.rpush(`soundcloud-sync:room:${room}:tracks`, JSON.stringify({ id: 1, html: 'test1' }));
      multi.rpush(`soundcloud-sync:room:${room}:tracks`, JSON.stringify({ id: 2, html: 'test2' }));
      multi.set(`soundcloud-sync:room:${room}:track-index`, 1);
      return multi.execAsync();
    }).then(() => done())
    .catch(err => done(err));
  });

  afterEach((done) => {
    sender.destroy();
    receiver.destroy();
    done();
  });

  after((done) => {
    client.flushallAsync().then(() => done())
      .catch(err => done(err));
  });

  it('should reset room info and set next track', (done) => {
    receiver.on('set next track', () => {
      multi.get(`soundcloud-sync:room:${room}:track-index`);
      multi.get(`soundcloud-sync:room:${room}:is-playing`);
      multi.get(`soundcloud-sync:room:${room}:initial-time`);
      multi.get(`soundcloud-sync:room:${room}:start-time`);
      multi.execAsync().then(([trackIndex, isPlaying, initialTime, startTime]) => {
        expect(parseInt(trackIndex, 10)).to.equal(0);
        expect(isPlaying).to.equal('1');
        expect(parseInt(initialTime, 10)).to.equal(0);
        expect(parseInt(startTime, 10)).to.be.closeTo(Date.now(), 2000);
        done();
      }).catch(err => done(err));
    });
    sender.emit('finish', data);
  });

  it('should emit set next track event to clients in the same room excluding sender', (done) => {
    const clientInDiffRoom = io.connect('http://localhost:8301');
    clientInDiffRoom.emit('join room', { room: 'test2' });
    clientInDiffRoom.on('set next track', () => {
      done(new Error('Client in different room received set next track event.'));
    });
    receiver.on('set next track', () => {
      clientInDiffRoom.destroy();
      done();
    });
    sender.on('set next track', () => {
      done(new Error('Sender received set next track event.'));
    });
    sender.emit('finish', data);
  });

  it('should send nextTrackIndex to receiver', (done) => {
    receiver.on('set next track', ({ nextTrackIndex }) => {
      expect(nextTrackIndex).to.equal(0);
      done();
    });
    sender.emit('finish', data);
  });
});
