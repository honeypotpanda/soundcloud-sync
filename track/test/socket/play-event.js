/* eslint no-unused-expressions: "off" */
/* global describe beforeEach afterEach after it */

const chai = require('chai');
const redis = require('redis');
const bluebird = require('bluebird');
const io = require('socket.io-client');

require('../../src/app');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const expect = chai.expect;
const client = redis.createClient({ port: 7000 });
const multi = client.multi();
let sender = null;
let receiver = null;
const room = 'test';
const data = {
  time: 1000,
  room,
};

describe('play event', () => {
  beforeEach((done) => {
    client.flushallAsync().then(() => {
      sender = io.connect('http://localhost:8301');
      receiver = io.connect('http://localhost:8301');
      sender.emit('join room', { room });
      receiver.emit('join room', { room });
      done();
    }).catch(err => done(err));
  });

  afterEach((done) => {
    sender.destroy();
    receiver.destroy();
    done();
  });

  after((done) => {
    client.flushallAsync().then(() => done())
      .catch(err => done(err));
  });

  it('should modify room info', (done) => {
    receiver.on('play', () => {
      multi.get(`soundcloud-sync:room:${room}:initial-time`);
      multi.get(`soundcloud-sync:room:${room}:start-time`);
      multi.get(`soundcloud-sync:room:${room}:is-playing`);
      multi.execAsync().then(([initialTime, startTime, isPlaying]) => {
        expect(parseInt(initialTime, 10)).to.equal(1000);
        expect(parseInt(startTime, 10)).to.be.closeTo(Date.now(), 2000);
        expect(isPlaying).to.equal('1');
        done();
      }).catch(err => done(err));
    });
    sender.emit('play', data);
  });

  it('should emit play event to clients in the same room excluding sender', (done) => {
    const clientInDiffRoom = io.connect('http://localhost:8301');
    clientInDiffRoom.emit('join room', { room: 'test2' });
    clientInDiffRoom.on('play', () => {
      done(new Error('Client in different room received play event.'));
    });
    sender.on('play', () => {
      done(new Error('Sender received play event.'));
    });
    receiver.on('play', () => {
      done();
    });
    sender.emit('play', data);
  });

  it('should send index and time to receiver', (done) => {
    receiver.on('play', ({ time }) => {
      expect(time).to.equal(1000);
      done();
    });
    sender.emit('play', data);
  });
});
