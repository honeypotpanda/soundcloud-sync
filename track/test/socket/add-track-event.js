/* eslint no-unused-expressions: "off" */
/* global describe beforeEach afterEach after it */

const chai = require('chai');
const redis = require('redis');
const bluebird = require('bluebird');
const io = require('socket.io-client');

require('../../src/app');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const expect = chai.expect;
const client = redis.createClient({ port: 7000 });
let sender = null;
let receiver = null;
const room = 'test';
const data = {
  track: {
    id: 1,
    html: 'test',
  },
  room,
};

describe('add track event', () => {
  beforeEach((done) => {
    client.flushallAsync().then(() => {
      sender = io.connect('http://localhost:8301');
      receiver = io.connect('http://localhost:8301');
      sender.emit('join room', { room });
      receiver.emit('join room', { room });
      done();
    }).catch(err => done(err));
  });

  afterEach((done) => {
    sender.destroy();
    receiver.destroy();
    done();
  });

  after((done) => {
    client.flushallAsync().then(() => done())
      .catch(err => done(err));
  });

  it('should emit add track event to clients in the same room excluding sender', (done) => {
    const clientInDiffRoom = io.connect('http://localhost:8301');
    clientInDiffRoom.emit('join room', { room: 'test2' });
    clientInDiffRoom.on('add track', () => {
      done(new Error('Client in different room received add track event.'));
    });
    sender.on('add track', () => {
      done(new Error('Sender received add track event.'));
    });
    receiver.on('add track', () => {
      done();
    });
    sender.emit('add track', data);
  });

  it('should send track to receiver', (done) => {
    receiver.on('add track', ({ id, html }) => {
      expect(id).to.equal(1);
      expect(html).to.equal('test');
      done();
    });
    sender.emit('add track', data);
  });
});
