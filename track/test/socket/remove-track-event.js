/* eslint no-unused-expressions: "off" */
/* global describe beforeEach afterEach after it */

const chai = require('chai');
const redis = require('redis');
const bluebird = require('bluebird');
const io = require('socket.io-client');

require('../../src/app');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const expect = chai.expect;
const client = redis.createClient({ port: 7000 });
let sender = null;
let receiver = null;
const room = 'test';
const data = {
  index: 0,
  room,
};

describe('remove track event', () => {
  beforeEach((done) => {
    client.flushallAsync().then(() => {
      sender = io.connect('http://localhost:8301');
      receiver = io.connect('http://localhost:8301');
      sender.emit('join room', { room });
      receiver.emit('join room', { room });
      done();
    }).catch(err => done(err));
  });

  afterEach((done) => {
    sender.destroy();
    receiver.destroy();
    done();
  });

  after((done) => {
    client.flushallAsync().then(() => done())
      .catch(err => done(err));
  });

  it('should emit remove track event to clients in the same room excluding sender', (done) => {
    const clientInDiffRoom = io.connect('http://localhost:8301');
    clientInDiffRoom.emit('join room', { room: 'test2' });
    clientInDiffRoom.on('remove track', () => {
      done(new Error('Client in different room received remove track event.'));
    });
    sender.on('remove track', () => {
      done(new Error('Sender received remove track event.'));
    });
    receiver.on('remove track', () => {
      done();
    });
    sender.emit('remove track', data);
  });

  it('should send index to receiver', (done) => {
    receiver.on('remove track', ({ index }) => {
      expect(index).to.equal(0);
      done();
    });
    sender.emit('remove track', data);
  });
});
