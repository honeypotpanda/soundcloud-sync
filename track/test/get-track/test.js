/* eslint no-unused-expressions: "off" */
/* global describe before after it */

const chai = require('chai');
const chaiHttp = require('chai-http');
const nock = require('nock');
const { escape } = require('lodash');

const app = require('../../src/app');

const expect = chai.expect;
const url = 'http://soundcloud.com/forss/flickermood';

chai.use(chaiHttp);

describe('GET /api/tracks', () => {
  it('should get track', (done) => {
    nock('https://soundcloud.com')
      .get('/oembed')
      .query({
        format: 'json',
        url: escape(url),
        maxheight: 200,
      })
      .reply(200, {
        version: 1,
        type: 'rich',
        provider_name: 'SoundCloud',
        provider_url: 'http://soundcloud.com',
        height: 400,
        width: '100%',
        title: 'Flickermood by Forss',
        description: 'From the Soulhack album,&nbsp;recently featured in this ad <a href="https://www.dswshoes.com/tv_commercial.jsp?m=october2007">https://www.dswshoes.com/tv_commercial.jsp?m=october2007</a> ',
        thumbnail_url: 'http://i1.sndcdn.com/artworks-000067273316-smsiqx-t500x500.jpg',
        html: '<iframe width="100%" height="400" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?visual=true&url=https%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F293&show_artwork=true"></iframe>',
        author_name: 'Forss',
        author_url: 'https://soundcloud.com/forss',
      });
    chai
      .request(app)
      .get('/api/tracks')
      .query({ url })
      .end((err, res) => {
        expect(err).to.not.exist;
        expect(res.status).to.equal(200);
        expect(res.body.version).to.exist;
        expect(res.body.type).to.exist;
        expect(res.body.provider_name).to.exist;
        expect(res.body.provider_url).to.exist;
        expect(res.body.height).to.exist;
        expect(res.body.width).to.exist;
        expect(res.body.title).to.exist;
        expect(res.body.description).to.exist;
        expect(res.body.thumbnail_url).to.exist;
        expect(res.body.html).to.exist;
        expect(res.body.author_name).to.exist;
        expect(res.body.author_url).to.exist;
        done();
      });
  });

  it('should not get track without a SoundCloud URL', (done) => {
    chai
      .request(app)
      .get('/api/tracks')
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(400);
        expect(res.body.message).to.equal('URL is missing.');
        done();
      });
  });

  it('should not get track with an invalid SoundCloud URL', (done) => {
    chai
      .request(app)
      .get('/api/tracks')
      .query({ url: 'http://google.com' })
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(400);
        expect(res.body.message).to.equal('Invalid URL.');
        done();
      });
  });

  it('should not get track that does not exist', (done) => {
    nock('https://soundcloud.com')
      .get('/oembed')
      .query({
        format: 'json',
        url: escape(url),
        maxheight: 200,
      })
      .reply(404);
    chai
      .request(app)
      .get('/api/tracks')
      .query({ url })
      .end((err, res) => {
        expect(err).to.exist;
        expect(res.status).to.equal(404);
        expect(res.body).to.have.property('message');
        done();
      });
  });
});
