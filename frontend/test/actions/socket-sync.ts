import { expect } from 'chai';

import actions from '../../src/actions/socket';
import types from '../../src/action-types/socket';

describe('socket sync actions', () => {
    it('connectSocket should create CONNECT_SOCKET action', () => {
        expect(actions.connectSocket()).to.deep.equal({
            type: types.CONNECT_SOCKET,
        });
    });

    it('disconnectSocket should create DISCONNECT_SOCKET action', () => {
        expect(actions.disconnectSocket()).to.deep.equal({
            type: types.DISCONNECT_SOCKET,
        });
    });
});
