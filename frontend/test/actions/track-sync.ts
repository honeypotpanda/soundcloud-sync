import { expect } from 'chai';

import actions from '../../src/actions/track';
import types from '../../src/action-types/track';

describe('track sync actions', () => {
    it('addTrackRequest should create ADD_TRACK_REQUEST action', () => {
        expect(actions.addTrackRequest()).to.deep.equal({
            type: types.ADD_TRACK_REQUEST,
        });
    });

    it('addTrackSuccess should create ADD_TRACK_SUCCESS action', () => {
        const track = {
            id: '1',
            html: 'test',
            title: 'test',
            username: 'test',
            artwork_url: 'http://www.test.com',
            permalink_url: 'http://www.test.com',
        };
        expect(actions.addTrackSuccess(track)).to.deep.equal({
            type: types.ADD_TRACK_SUCCESS,
            track,
        });
    });

    it('addTrackFailure should create ADD_TRACK_FAILURE action', () => {
        const error = { message: 'Not found.' };
        expect(actions.addTrackFailure(error)).to.deep.equal({
            type: types.ADD_TRACK_FAILURE,
            error,
        });
    });

    it('resetTracks should create RESET_TRACKS action', () => {
        expect(actions.resetTracks()).to.deep.equal({
            type: types.RESET_TRACKS,
        });
    });

    it('removeTrackRequest should create REMOVE_TRACK_REQUEST action', () => {
        expect(actions.removeTrackRequest()).to.deep.equal({
            type: types.REMOVE_TRACK_REQUEST,
        });
    });

    it('removeTrackSuccess should create REMOVE_TRACK_SUCCESS action', () => {
        const data = {
            message: 'Successfully deleted track 1.',
            index: 0,
        };
        expect(actions.removeTrackSuccess(data)).to.deep.equal({
            type: types.REMOVE_TRACK_SUCCESS,
            data,
        });
    });

    it('removeTrackFailure should create REMOVE_TRACK_FAILURE action', () => {
        const error = {
            message: 'Unable to find track 1.',
        };
        expect(actions.removeTrackFailure(error)).to.deep.equal({
            type: types.REMOVE_TRACK_FAILURE,
            error,
        });
    });
});
