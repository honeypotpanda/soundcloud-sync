import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as nock from 'nock';
import { expect } from 'chai';

import actions from '../../src/actions/room';
import types from '../../src/action-types/room';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const initialState = {
    room: {
        isFetching: false,
        list: [],
        error: '',
    },
};
const response = {
    rooms: [
        { name: 'test1' },
        { name: 'test2' },
        { name: 'test3' },
    ],
};
const roomName = 'test';

describe('room async actions', () => {
    afterEach(() => {
        localStorage.clear();
        nock.cleanAll();
    });

    it('creates GET_ROOMS_SUCCESS when successfully fetching rooms', () => {
        const expectedActions = [
            { type: types.GET_ROOMS_REQUEST },
            {
                type: types.GET_ROOMS_SUCCESS,
                data: {
                    rooms: [
                        { name: 'test1' },
                        { name: 'test2' },
                        { name: 'test3' },
                    ],
                },
            },
        ];
        const store = mockStore(initialState);

        nock('http://localhost:8200')
            .get('/api/rooms')
            .reply(200, response);

        store.dispatch(actions.getRooms())
            .then(() => {
                expect(store.getActions()).to.deep.equal(expectedActions);
            });
    });

    it('creates GET_ROOMS_FAILURE when unsuccessfully fetching rooms', () => {
        const expectedActions = [
            { type: types.GET_ROOMS_REQUEST },
            { type: types.GET_ROOMS_FAILURE, error: { message: 'Something went wrong...' } },
        ];
        const store = mockStore(initialState);

        nock('http://localhost:8200')
            .get('/api/rooms')
            .reply(400, { message: 'Something went wrong...' });

        store.dispatch(actions.getRooms())
            .then(() => {
                expect(store.getActions()).to.deep.equal(expectedActions);
            });
    });

    it('creates POST_ROOM_SUCCESS when successfully creating a room', () => {
        const expectedActions = [
            { type: types.POST_ROOM_REQUEST },
            { type: types.POST_ROOM_SUCCESS, room: 'test' },
        ];
        const store = mockStore(initialState);

        nock('http://localhost:8200')
            .post('/api/rooms')
            .reply(200, { message: 'Successfully created room.' });

        store.dispatch(actions.postRoom(roomName))
            .then(() => {
                expect(store.getActions()).to.deep.equal(expectedActions);
            });
    });

    it('creates POST_ROOM_FAILURE when unsuccessfully creating a room', () => {
        const expectedActions = [
            { type: types.POST_ROOM_REQUEST },
            { type: types.POST_ROOM_FAILURE, error: { message: 'Something went wrong...' } },
        ];
        const store = mockStore(initialState);

        nock('http://localhost:8200')
            .post('/api/rooms')
            .reply(400, { message: 'Something went wrong...' });

        store.dispatch(actions.postRoom(roomName))
            .then(() => {
                expect(store.getActions()).to.deep.equal(expectedActions);
            });
    });

    it('creates GET_ROOM_INFO_SUCCESS when successfully fetching room info', () => {
        const response = {
            tracks: [],
            initialTime: 0,
            startTime: 0,
            isPlaying: false,
            trackIndex: 0,
        };
        const expectedActions = [
            { type: types.GET_ROOM_INFO_REQUEST },
            { type: types.GET_ROOM_INFO_SUCCESS, info: response },
        ];
        const store = mockStore(initialState);

        nock('http://localhost:8200')
            .get(`/api/rooms/${roomName}`)
            .reply(200, response);

        store.dispatch(actions.getRoomInfo(roomName))
            .then(() => {
                expect(store.getActions()).to.deep.equal(expectedActions);
            });
    });

    it('creates GET_ROOM_INFO_FAILURE when unsuccessfully fetching room info', () => {
        const expectedActions = [
            { type: types.GET_ROOM_INFO_REQUEST },
            { type: types.GET_ROOM_INFO_FAILURE, error: { message: 'Something went wrong...' } },
        ];
        const store = mockStore(initialState);

        nock('http://localhost:8200')
            .get(`/api/rooms/${roomName}`)
            .reply(400, { message: 'Something went wrong...' });

        store.dispatch(actions.getRoomInfo(roomName))
            .then(() => {
                expect(store.getActions()).to.deep.equal(expectedActions);
            });
    });
});
