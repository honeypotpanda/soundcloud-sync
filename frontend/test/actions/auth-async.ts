import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as nock from 'nock';
import { expect } from 'chai';

import actions from '../../src/actions/auth';
import types from '../../src/action-types/auth';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const initialState = {
    auth: {
        isFetching: false,
        isLoggedIn: false,
        error: '',
    },
};
const account = {
    username: 'test',
    email: 'test@test.com',
    password: 'test',
    passwordConfirm: 'test',
};

describe('auth async actions', () => {
    afterEach(() => {
        localStorage.clear();
        nock.cleanAll();
    });

    it('creates POST_AUTH_SUCCESS when successfully signing up', () => {
        const expectedActions = [
            { type: types.POST_AUTH_REQUEST },
            { type: types.POST_AUTH_SUCCESS },
        ];
        const store = mockStore(initialState);

        nock('http://localhost:8100')
            .post('/auth/signup')
            .reply(200, { soundcloud_sync_token: 'token' });

        store.dispatch(actions.postSignup(account))
            .then(() => {
                expect(store.getActions()).to.deep.equal(expectedActions);
            });
    });

    it('stores soundcloud_sync_token in localStorage when successfully signing up', () => {
        const store = mockStore(initialState);

        nock('http://localhost:8100')
            .post('/auth/signup')
            .reply(200, { soundcloud_sync_token: 'token' });

        store.dispatch(actions.postSignup(account))
            .then(() => {
                expect(localStorage.getItem('soundcloud_sync_token')).to.exist;
            });
    });

    it('creates POST_AUTH_FAILURE when unsucessfully signing up', () => {
        const error = { message: 'Email has already been registered.' };
        const expectedActions = [
            { type: types.POST_AUTH_REQUEST },
            { type: types.POST_AUTH_FAILURE, error },
        ];
        const store = mockStore(initialState);

        nock('http://localhost:8100')
            .post('/auth/signup')
            .reply(400, error);

        store.dispatch(actions.postSignup(account))
            .then(() => {
                expect(store.getActions()).to.deep.equal(expectedActions);
            });
    });

    it('creates POST_AUTH_SUCCESS when successfully signing in', () => {
        const expectedActions = [
            { type: types.POST_AUTH_REQUEST },
            { type: types.POST_AUTH_SUCCESS },
        ];
        const store = mockStore(initialState);

        nock('http://localhost:8100')
            .post('/auth/signin')
            .reply(200, { soundcloud_sync_token: 'token' });

        store.dispatch(actions.postSignin(account))
            .then(() => {
                expect(store.getActions()).to.deep.equal(expectedActions);
            });
    });

    it('stores soundcloud_sync_token in localStorage when successfully signing in', () => {
        const store = mockStore(initialState);

        nock('http://localhost:8100')
            .post('/auth/signin')
            .reply(200, { soundcloud_sync_token: 'token' });

        store.dispatch(actions.postSignin(account))
            .then(() => {
                expect(localStorage.getItem('soundcloud_sync_token')).to.exist;
            });
    });

    it('creates POST_AUTH_FAILURE when unsucessfully signing in', () => {
        const error = { message: 'Email has already been registered.' };
        const expectedActions = [
            { type: types.POST_AUTH_REQUEST },
            { type: types.POST_AUTH_FAILURE, error },
        ];
        const store = mockStore(initialState);

        nock('http://localhost:8100')
            .post('/auth/signin')
            .reply(400, error);

        store.dispatch(actions.postSignin(account))
            .then(() => {
                expect(store.getActions()).to.deep.equal(expectedActions);
            });
    });
});
