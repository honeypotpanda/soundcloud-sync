import { expect } from 'chai';

import actions from '../../src/actions/room';
import types from '../../src/action-types/room';

describe('room sync actions', () => {
    it('getRoomsRequest should create GET_ROOMS_REQUEST action', () => {
        expect(actions.getRoomsRequest()).to.deep.equal({
            type: types.GET_ROOMS_REQUEST,
        });
    });

    it('getRoomsSuccess should create GET_ROOMS_SUCCESS action', () => {
        const data = {
            rooms: [
                { name: 'test1' },
                { name: 'test2' },
            ],
        };
        expect(actions.getRoomsSuccess(data)).to.deep.equal({
            type: types.GET_ROOMS_SUCCESS,
            data,
        });
    });

    it('getRoomsFailure should create GET_ROOMS_FAILURE action', () => {
        expect(actions.getRoomsFailure({ message: 'Something went wrong...' })).to.deep.equal({
            type: types.GET_ROOMS_FAILURE,
            error: { message: 'Something went wrong...' },
        });
    });

    it('postRoomRequest should create POST_ROOM_REQUEST action', () => {
        expect(actions.postRoomRequest()).to.deep.equal({
            type: types.POST_ROOM_REQUEST,
        });
    });

    it('postRoomSuccess should create POST_ROOM_SUCCESS action', () => {
        expect(actions.postRoomSuccess('test')).to.deep.equal({
            type: types.POST_ROOM_SUCCESS,
            room: 'test',
        });
    });

    it('postRoomFailure should create POST_ROOM_FAILURE action', () => {
        expect(actions.postRoomFailure({ message: 'Something went wrong...' })).to.deep.equal({
            type: types.POST_ROOM_FAILURE,
            error: { message: 'Something went wrong...' },
        });
    });

    it('getRoomInfoRequest should create GET_ROOM_INFO_REQUEST action', () => {
        expect(actions.getRoomInfoRequest()).to.deep.equal({
            type: types.GET_ROOM_INFO_REQUEST,
        });
    });

    it('getRoomInfoSuccess should create GET_ROOM_INFO_SUCCESS action', () => {
        const info = {
            tracks: [],
            initialTime: 0,
            startTime: 0,
            isPlaying: false,
            trackIndex: 0,
        };
        expect(actions.getRoomInfoSuccess(info)).to.deep.equal({
            type: types.GET_ROOM_INFO_SUCCESS,
            info: info,
        });
    });

    it('getRoomInfoFailure should create GET_ROOM_INFO_FAILURE action', () => {
        const error = {
            message: 'Something went wrong...',
        };
        expect(actions.getRoomInfoFailure(error)).to.deep.equal({
            type: types.GET_ROOM_INFO_FAILURE,
            error,
        });
    });

    it('setTrackIndex should create SET_TRACK_INDEX action', () => {
        expect(actions.setTrackIndex(1)).to.deep.equal({
            type: types.SET_TRACK_INDEX,
            index: 1,
        });
    });
});
