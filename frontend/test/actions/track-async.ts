import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as nock from 'nock';
import { expect } from 'chai';

import actions from '../../src/actions/track';
import types from '../../src/action-types/track';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const initialState = {
    track: {
        isFetching: false,
        list: [],
        error: '',
    },
};
const room = 'test';
const url = 'http://soundcloud.com/forss/flickermood';
const response = {
  "version": 1,
  "type": "rich",
  "provider_name": "SoundCloud",
  "provider_url": "http://soundcloud.com",
  "height": 400,
  "width": "100%",
  "title": "Flickermood by Forss",
  "description": "From the Soulhack album,&nbsp;recently featured in this ad <a href=\"https://www.dswshoes.com/tv_commercial.jsp?m=october2007\">https://www.dswshoes.com/tv_commercial.jsp?m=october2007</a> ",
  "thumbnail_url": "http://i1.sndcdn.com/artworks-000067273316-smsiqx-t500x500.jpg",
  "html": "<iframe width=\"100%\" height=\"400\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?visual=true&url=https%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F293&show_artwork=true\"></iframe>",
  "author_name": "Forss",
  "author_url": "https://soundcloud.com/forss",
  "id": "1234",
};
const error = {
    message: 'Not found.',
};
const id = '1';

describe('track async actions', () => {
    afterEach(() => {
        localStorage.clear();
        nock.cleanAll();
    });

    it('creates ADD_TRACK_SUCCESS when successfully adding track', () => {
        const expectedActions = [
            { type: types.ADD_TRACK_REQUEST },
            {
                type: types.ADD_TRACK_SUCCESS,
                track: response,
            },
        ];
        const store = mockStore(initialState);

        nock('http://localhost:8300')
            .post('/api/tracks', { room, url })
            .reply(200, response);

        store.dispatch(actions.addTrack(room, url))
            .then(() => {
                expect(store.getActions()).to.deep.equal(expectedActions);
            });
    });

    it('creates ADD_TRACK_FAILURE when unsuccessfully adding track', () => {
        const expectedActions = [
            { type: types.ADD_TRACK_REQUEST },
            {
                type: types.ADD_TRACK_FAILURE,
                error,
            },
        ];
        const store = mockStore(initialState);

        nock('http://localhost:8300')
            .post('/api/tracks', { room, url })
            .reply(404, error);

        store.dispatch(actions.addTrack(room, url))
            .then(() => {
                expect(store.getActions()).to.deep.equal(expectedActions);
            });
    });

    it('creates REMOVE_TRACK_SUCCESS when successfully removing track', () => {
        const removeTrackResponse = {
            message: `Successfully deleted track ${id}.`,
            index: 0,
        };
        const expectedActions = [
            { type: types.REMOVE_TRACK_REQUEST },
            {
                type: types.REMOVE_TRACK_SUCCESS,
                data: removeTrackResponse,
            },
        ];
        const store = mockStore(initialState);

        nock('http://localhost:8300')
            .delete(`/api/rooms/${room}/tracks/${id}`)
            .reply(200, removeTrackResponse);

        store.dispatch(actions.removeTrack(room, id))
            .then(() => {
                expect(store.getActions()).to.deep.equal(expectedActions);
            });
    });

    it('creates REMOVE_TRACK_FAILURE when unsuccessfully removing track', () => {
        const removeTrackError = {
            message: `Unable to find track ${id}.`,
        };
        const expectedActions = [
            { type: types.REMOVE_TRACK_REQUEST },
            {
                type: types.REMOVE_TRACK_FAILURE,
                error: removeTrackError,
            },
        ];
        const store = mockStore(initialState);

        nock('http://localhost:8300')
            .delete(`/api/rooms/${room}/tracks/${id}`)
            .reply(404, removeTrackError);

        store.dispatch(actions.removeTrack(room, id))
            .then(() => {
                expect(store.getActions()).to.deep.equal(expectedActions);
            });
    });
});
