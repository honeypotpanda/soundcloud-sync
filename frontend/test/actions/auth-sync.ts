import { expect } from 'chai';

import actions from '../../src/actions/auth';
import types from '../../src/action-types/auth';

describe('auth sync actions', () => {
    it('postAuthRequest should create POST_AUTH_REQUEST action', () => {
        expect(actions.postAuthRequest()).to.deep.equal({
            type: types.POST_AUTH_REQUEST,
        });
    });

    it('postAuthSuccess should create POST_AUTH_SUCCESS action', () => {
        expect(actions.postAuthSuccess()).to.deep.equal({
            type: types.POST_AUTH_SUCCESS,
        });
    });

    it('postAuthFailure should create POST_AUTH_FAILURE action', () => {
        const error = { message: 'Something went wrong...' };
        expect(actions.postAuthFailure(error)).to.deep.equal({
            type: types.POST_AUTH_FAILURE,
            error,
        });
    });

    it('setAuthError should create SET_AUTH_ERROR action', () => {
        const error = { message: 'Something went wrong...' };
        expect(actions.setAuthError(error)).to.deep.equal({
            type: types.SET_AUTH_ERROR,
            error,
        });
    });

    it('signOut should create SIGN_OUT action', () => {
        expect(actions.signOut()).to.deep.equal({
            type: types.SIGN_OUT,
        });
    });
});
