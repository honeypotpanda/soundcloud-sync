import { expect } from 'chai';

import reducer from '../../src/reducers/room';
import types from '../../src/action-types/room';

describe('room reducer', () => {
    it('should return the initial state', () => {
        const stateBefore = undefined;
        const action = {};
        const stateAfter = {
            isFetching: false,
            list: [],
            error: '',
            trackIndex: 0,
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle GET_ROOMS_REQUEST', () => {
        const stateBefore = {
            isFetching: false,
            list: [],
            error: 'Something went wrong...',
            trackIndex: 0,
        };
        const action = {
            type: types.GET_ROOMS_REQUEST,
        };
        const stateAfter = {
            isFetching: true,
            list: [],
            error: '',
            trackIndex: 0,
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle GET_ROOMS_SUCCESS', () => {
        const stateBefore = {
            isFetching: true,
            list: [],
            error: '',
            trackIndex: 0,
        };
        const action = {
            type: types.GET_ROOMS_SUCCESS,
            data: {
                rooms: [
                    { name: 'test1' },
                    { name: 'test2' },
                    { name: 'test3' },
                ],
            },
        };
        const stateAfter = {
            isFetching: false,
            list: ['test1', 'test2', 'test3'],
            error: '',
            trackIndex: 0,
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle GET_ROOMS_FAILURE', () => {
        const stateBefore = {
            isFetching: true,
            list: [],
            error: '',
            trackIndex: 0,
        };
        const action = {
            type: types.GET_ROOMS_FAILURE,
            error: { message: 'Something went wrong...' },
        };
        const stateAfter = {
            isFetching: false,
            list: [],
            error: 'Something went wrong...',
            trackIndex: 0,
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle POST_ROOM_REQUEST', () => {
        const stateBefore = {
            isFetching: false,
            list: [],
            error: 'Something went wrong...',
            trackIndex: 0,
        };
        const action = {
            type: types.POST_ROOM_REQUEST,
        };
        const stateAfter = {
            isFetching: true,
            list: [],
            error: '',
            trackIndex: 0,
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle POST_ROOM_SUCCESS', () => {
        const stateBefore = {
            isFetching: true,
            list: ['test1', 'test2'],
            error: '',
            trackIndex: 0,
        };
        const action = {
            type: types.POST_ROOM_SUCCESS,
            room: 'test3',
        };
        const stateAfter = {
            isFetching: false,
            list: ['test1', 'test2', 'test3'],
            error: '',
            trackIndex: 0,
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle POST_ROOM_FAILURE', () => {
        const stateBefore = {
            isFetching: true,
            list: [],
            error: '',
            trackIndex: 0,
        };
        const action = {
            type: types.POST_ROOM_FAILURE,
            error: { message: 'Something went wrong...' },
        };
        const stateAfter = {
            isFetching: false,
            list: [],
            error: 'Something went wrong...',
            trackIndex: 0,
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle GET_ROOM_INFO_REQUEST', () => {
        const stateBefore = {
            isFetching: false,
            list: [],
            error: 'Something went wrong...',
            trackIndex: 0,
        };
        const action = {
            type: types.GET_ROOM_INFO_REQUEST,
        };
        const stateAfter = {
            isFetching: true,
            list: [],
            error: '',
            trackIndex: 0,
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle GET_ROOM_INFO_SUCCESS', () => {
        const info = {
            tracks: [],
            initialTime: 0,
            startTime: 0,
            isPlaying: false,
            trackIndex: 0,
        };
        const stateBefore = {
            isFetching: true,
            list: [],
            error: '',
            trackIndex: 1,
        };
        const action = {
            type: types.GET_ROOM_INFO_SUCCESS,
            info,
        };
        const stateAfter = {
            isFetching: false,
            list: [],
            error: '',
            trackIndex: 0,
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle GET_ROOM_INFO_FAILURE', () => {
        const error = {
            message: 'Something went wrong...',
        };
        const stateBefore = {
            isFetching: true,
            list: [],
            error: '',
            trackIndex: 0,
        };
        const action = {
            type: types.GET_ROOM_INFO_FAILURE,
            error,
        };
        const stateAfter = {
            isFetching: false,
            list: [],
            error: 'Something went wrong...',
            trackIndex: 0,
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle SET_TRACK_INDEX', () => {
        const stateBefore = {
            isFetching: false,
            list: [],
            error: '',
            trackIndex: 0,
        };
        const action = {
            type: types.SET_TRACK_INDEX,
            index: 1,
        };
        const stateAfter = {
            isFetching: false,
            list: [],
            error: '',
            trackIndex: 1,
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });
});
