import { expect } from 'chai';

import reducer from '../../src/reducers/auth';
import types from '../../src/action-types/auth';

describe('auth reducer', () => {
    it('should return the initial state', () => {
        const stateBefore = undefined;
        const action = {};
        const stateAfter = {
            isFetching: false,
            isLoggedIn: false,
            error: '',
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle POST_AUTH_REQUEST', () => {
        const stateBefore = {
            isFetching: false,
            isLoggedIn: false,
            error: 'Something went wrong...',
        };
        const action = {
            type: types.POST_AUTH_REQUEST,
        };
        const stateAfter = {
            isFetching: true,
            isLoggedIn: false,
            error: '',
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle POST_AUTH_SUCCESS', () => {
        const stateBefore = {
            isFetching: true,
            isLoggedIn: false,
            error: '',
        };
        const action = {
            type: types.POST_AUTH_SUCCESS,
        };
        const stateAfter = {
            isFetching: false,
            isLoggedIn: true,
            error: '',
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle POST_AUTH_FAILURE', () => {
        const stateBefore = {
            isFetching: true,
            isLoggedIn: false,
            error: '',
        };
        const action = {
            type: types.POST_AUTH_FAILURE,
            error: { message: 'Something went wrong...' },
        };
        const stateAfter = {
            isFetching: false,
            isLoggedIn: false,
            error: 'Something went wrong...',
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle SET_AUTH_ERROR', () => {
        const stateBefore = {
            isFetching: false,
            isLoggedIn: false,
            error: '',
        };
        const action = {
            type: types.SET_AUTH_ERROR,
            error: { message: 'Something went wrong...' },
        };
        const stateAfter = {
            isFetching: false,
            isLoggedIn: false,
            error: 'Something went wrong...',
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle SIGN_OUT', () => {
        const stateBefore = {
            isFetching: true,
            isLoggedIn: true,
            error: 'Something went wrong...',
        };
        const action = {
            type: types.SIGN_OUT,
        };
        const stateAfter = {
            isFetching: false,
            isLoggedIn: false,
            error: '',
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });
});
