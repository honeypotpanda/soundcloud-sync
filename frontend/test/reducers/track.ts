import { expect } from 'chai';

import reducer from '../../src/reducers/track';
import trackTypes from '../../src/action-types/track';
import roomTypes from '../../src/action-types/room';

describe('track reducer', () => {
    const track1 = {
        id: '1',
        html: 'test1',
        title: 'test1',
        username: 'test1',
        artwork_url: 'http://www.test1.com',
        permalink_url: 'http://www.test1.com',
    };
    const track2 = {
        id: '2',
        html: 'test2',
        title: 'test2',
        username: 'test2',
        artwork_url: 'http://www.test2.com',
        permalink_url: 'http://www.test2.com',
    };

    it('should return the initial state', () => {
        const stateBefore = undefined;
        const action = {};
        const stateAfter = {
            isFetching: false,
            list: [],
            error: '',
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle ADD_TRACK_REQUEST', () => {
        const stateBefore = {
            isFetching: false,
            list: [],
            error: 'Something went wrong...',
        };
        const action = {
            type: trackTypes.ADD_TRACK_REQUEST,
        };
        const stateAfter = {
            isFetching: true,
            list: [],
            error: '',
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle ADD_TRACK_SUCCESS', () => {
        const stateBefore = {
            isFetching: true,
            list: [],
            error: '',
        };
        const action = {
            type: trackTypes.ADD_TRACK_SUCCESS,
            track: track1,
        };
        const stateAfter = {
            isFetching: false,
            list: [track1],
            error: '',
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle ADD_TRACK_FAILURE', () => {
        const error = {
            message: 'Not found.',
        };
        const stateBefore = {
            isFetching: true,
            list: [],
            error: '',
        };
        const action = {
            type: trackTypes.ADD_TRACK_FAILURE,
            error,
        };
        const stateAfter = {
            isFetching: false,
            list: [],
            error: 'Not found.',
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle RESET_TRACKS', () => {
        const stateBefore = {
            isFetching: false,
            list: [track1, track2],
            error: '',
        };
        const action = {
            type: trackTypes.RESET_TRACKS,
        };
        const stateAfter = {
            isFetching: false,
            list: [],
            error: '',
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle REMOVE_TRACK_REQUEST', () => {
        const stateBefore = {
            isFetching: false,
            list: [],
            error: 'Something went wrong...',
        };
        const action = {
            type: trackTypes.REMOVE_TRACK_REQUEST,
        };
        const stateAfter = {
            isFetching: true,
            list: [],
            error: '',
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle REMOVE_TRACK_SUCCESS', () => {
        const stateBefore = {
            isFetching: true,
            list: [track1, track2],
            error: '',
        };
        const action = {
            type: trackTypes.REMOVE_TRACK_SUCCESS,
            data: {
                message: 'Successfully deleted track 0.',
                index: 0,
            },
        };
        const stateAfter = {
            isFetching: false,
            list: [track2],
            error: '',
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle REMOVE_TRACK_FAILURE', () => {
        const stateBefore = {
            isFetching: true,
            list: [track1, track2],
            error: '',
        };
        const action = {
            type: trackTypes.REMOVE_TRACK_FAILURE,
            error: {
                message: 'Unable to find track 0.'
            },
        };
        const stateAfter = {
            isFetching: false,
            list: [track1, track2],
            error: 'Unable to find track 0.',
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });

    it('should handle GET_ROOM_INFO_SUCCESS', () => {
        const tracks = [track1, track2];
        const info = {
            tracks,
            initialTime: 0,
            startTime: 0,
            isPlaying: false,
            trackIndex: 0,
        };
        const stateBefore = {
            isFetching: false,
            list: [],
            error: '',
        };
        const action = {
            type: roomTypes.GET_ROOM_INFO_SUCCESS,
            info,
        };
        const stateAfter = {
            isFetching: false,
            list: [track1, track2],
            error: '',
        };

        expect(
            reducer(stateBefore, action)
        ).to.deep.equal(stateAfter);
    });
});
