import { expect } from 'chai';
import * as io from 'socket.io-client';

import reducer from '../../src/reducers/socket';
import types from '../../src/action-types/socket';

describe('socket reducer', () => {
    it('should return the initial state', () => {
        const stateBefore = undefined;
        const action = {};
        const stateAfter = null;

        expect(
            reducer(stateBefore, action)
        ).to.equal(stateAfter);
    });

    it('should handle CONNECT_SOCKET', () => {
        const stateBefore = null;
        const action = {
            type: types.CONNECT_SOCKET,
        };

        expect(reducer(stateBefore, action)).to.not.be.null;
    });

    it('should handle DISCONNECT_SOCKET', () => {
        const stateBefore = io.connect('http://localhost:8300');
        const action = {
            type: types.DISCONNECT_SOCKET,
        };

        expect(reducer(stateBefore, action)).to.be.null;
    });
});
