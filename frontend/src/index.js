import 'semantic-ui-react';
import 'semantic-ui-css/semantic.css';
import '../lib/soundcloud/widget';
import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';

import App from './containers/App';
import reducer from './reducers';
import { authenticateUser } from './actions/auth';

const logger = createLogger();
const store = createStore(
  reducer,
  applyMiddleware(thunk, logger)
);

if (localStorage.getItem('accessToken')) {
  store.dispatch(authenticateUser());
}

ReactDOM.render(
  <AppContainer>
    <Provider store={store}>
      <App />
    </Provider>
  </AppContainer>,
  document.getElementById('app')
);

if (module.hot) {
  module.hot.accept('./containers/App', () => {
    const NewRoot = require('./containers/App').default;
    ReactDOM.render(
      <AppContainer>
        <Provider store={store}>
          <NewRoot />
        </Provider>
      </AppContainer>,
      document.getElementById('app')
    );
  });
}
