import React from 'react';
import { connect } from 'react-redux';
import { Button, Form, Modal } from 'semantic-ui-react';

import { ADD_TRACK_SUCCESS } from '../action-types/track';
import { addTrack } from '../actions/track';

export class AddTrack extends React.Component {
  static contextTypes = {
    router: React.PropTypes.object.isRequired,
  };

  room = this.context.router.params.roomName;
  state = { open: false };
  urlInput = null;

  show = () => this.setState({ open: true });
  close = () => this.setState({ open: false });

  handleSubmit = (e) => {
    e.preventDefault();
    const { addTrack, socket } = this.props;
    const url = this.urlInput.value;
    addTrack(this.room, url)
    .then((action) => {
      if (action.type === ADD_TRACK_SUCCESS) {
        socket.emit('add track', {
          track: action.track,
          room: this.room,
        });
        this.close();
      }
    });
    this.urlInput.value = '';
  }

  render() {
    return (
      <div>
        <Button size="small" onClick={this.show}>Add Track</Button>
        <Modal size="small" open={this.state.open} onClose={this.close}>
          <Modal.Header>
            Add Track
          </Modal.Header>
          <Modal.Content>
            <Form onSubmit={this.handleSubmit}>
              <Form.Field>
                <input
                  type="url"
                  name="url"
                  placeholder="SoundCloud URL"
                  ref={input => this.urlInput = input}
                  required
                />
              </Form.Field>
              <Button primary type="submit">Submit</Button>
              <Button onClick={this.close}>Cancel</Button>
            </Form>
          </Modal.Content>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  socket: state.socket,
});

const mapDispatchToProps = dispatch => ({
  addTrack: (room, url) => dispatch(addTrack(room, url)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddTrack);
