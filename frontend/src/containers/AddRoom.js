import React from 'react';
import { connect } from 'react-redux';
import { Button, Form, Header, Input, Menu, Modal } from 'semantic-ui-react';

import { POST_ROOM_SUCCESS } from '../action-types/room';
import { postRoom } from '../actions/room';

export class AddRoom extends React.Component {
  static contextTypes = {
    router: React.PropTypes.object.isRequired,
  };

  state = { open: false };
  roomInput = null;

  show = () => this.setState({ open: true });
  close = () => this.setState({ open: false });

  handleSubmit = (e) => {
    e.preventDefault();
    const { postRoom } = this.props;
    const roomName = this.roomInput.value;
    postRoom(roomName)
    .then((action) => {
      if (action.type === POST_ROOM_SUCCESS) {
        this.close();
      }
    });
    this.roomInput.value = '';
  }

  render() {
    const { isLoggedIn } = this.props;
    return (
      <div>
        {isLoggedIn &&
          <Button size="small" onClick={this.show}>
            Create Room
          </Button>
        }
        <Modal size="small" open={this.state.open} onClose={this.close}>
          <Modal.Header>
            Create Room
          </Modal.Header>
          <Modal.Content>
            <Form onSubmit={this.handleSubmit}>
              <Form.Field>
                <input
                  type="text"
                  name="roomName"
                  placeholder="Room name"
                  ref={input => this.roomInput = input}
                  required
                />
              </Form.Field>
              <Button primary type="submit">Submit</Button>
              <Button onClick={this.close}>Cancel</Button>
            </Form>
          </Modal.Content>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoggedIn: state.auth.isLoggedIn,
});

const mapDispatchToProps = dispatch => ({
  postRoom: function(roomName) {
    return dispatch(postRoom(roomName));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddRoom);
