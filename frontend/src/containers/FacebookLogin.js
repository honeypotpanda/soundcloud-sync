import React from 'react';
import { connect } from 'react-redux';
import { Menu } from 'semantic-ui-react';

import { facebookLoginSuccess } from '../actions/auth';

export class FacebookLogin extends React.Component {
  handleClick = () => {
    FB.login(({ authResponse, status }) => {
      if (status === 'connected') {
        this.props.facebookLoginSuccess();
        localStorage.setItem('accessToken', authResponse.accessToken);
        localStorage.setItem('userID', authResponse.userID);
      }
    });
  };

  render() {
    return (
      <Menu.Item onClick={this.handleClick}>Log in with Facebook</Menu.Item>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  facebookLoginSuccess: () => dispatch(facebookLoginSuccess()),
});

export default connect(
  null,
  mapDispatchToProps
)(FacebookLogin);
