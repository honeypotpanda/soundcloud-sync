import React from 'react';
import { connect } from 'react-redux';
import { Header, List } from 'semantic-ui-react';

import { getRoomInfo } from '../actions/room';
import {
  addTrackSuccess,
  removeTrack,
  removeTrackSuccess
} from '../actions/track';
import Track from '../components/Track';

export class TrackList extends React.Component {
  static contextTypes = {
    router: React.PropTypes.object.isRequired,
  };

  room = this.context.router.params.roomName;

  handleRemoveClick = (room, id) => {
    const { removeTrack, socket } = this.props;
    removeTrack(room, id)
    .then(({ data }) => {
      socket.emit('remove track', {
        index: data.index,
        room,
      });
    });
  }

  componentDidMount() {
    const {
      addTrack,
      getRoomInfo,
      removeTrackSuccess,
      socket
    } = this.props;
    getRoomInfo(this.room);
    socket.on('add track', (track) => {
      addTrack(track);
    });
    socket.on('remove track', ({ index }) => {
      removeTrackSuccess(index);
    });
  }

  render() {
    const { tracks } = this.props;
    return (
      <List verticalAlign="middle">
        <List.Header><Header as ="h4">Tracklist</Header></List.Header>
        {tracks.map((track) => (
          <Track
            key={track.id}
            artworkUrl={track.artwork_url}
            permalinkUrl={track.permalink_url}
            title={track.title}
            username={track.username}
            onRemoveClick={() => this.handleRemoveClick(this.room, track.id)}
            isActive={false}
          />
        ))}
      </List>
    );
  }
}

const mapStateToProps = state => ({
  socket: state.socket,
  tracks: state.track.list,
});

const mapDispatchToProps = dispatch => ({
  addTrack: (track) => dispatch(addTrackSuccess(track)),
  getRoomInfo: (room) => dispatch(getRoomInfo(room)),
  removeTrack: (room, id) => dispatch(removeTrack(room, id)),
  removeTrackSuccess: (index) => dispatch(removeTrackSuccess({ index, message: '' })),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TrackList);
