import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import {
  Button,
  Container,
  Form,
  Icon,
  Message,
} from 'semantic-ui-react';

import { postSignup, setAuthError } from '../actions/auth';

export class Signup extends React.Component {
  static contextTypes = {
    router: React.PropTypes.object.isRequired,
  }

  state = {
    formData: {
      username: '',
      email: '',
      password: '',
      passwordConfirm: '',
    },
  };

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.isLoggedIn) {
      this.context.router.push('/');
    }
  }

  handleSubmit = (e, { formData }) => {
    e.preventDefault();
    const { postSignup, setAuthError } = this.props;
    if (formData.password !== formData.passwordConfirm) {
      return setAuthError({ message: 'Passwords do not match.' });
    }
    postSignup(formData);
  }

  render() {
    const { error } = this.props;
    return (
      <Container text>
        <Message
          attached
          header="Welcome to SoundCloud Sync!"
          content="Please fill out the form below to sign up for a new account"
        />
        <Form className="attached fluid segment" onSubmit={this.handleSubmit}>
          <Message
            error
            header='An error ocurred'
            content={error}
            visible={error !== ''}
          />
          <Form.Field>
            <label>Username</label>
            <input type="text" name="username" required />
          </Form.Field>
          <Form.Field>
            <label>Email</label>
            <input type="email" name="email" required />
          </Form.Field>
          <Form.Field>
            <label>Password</label>
            <input type="password" name="password" required />
          </Form.Field>
          <Form.Field>
            <label>Confirm your password</label>
            <input type="password" name="passwordConfirm" required />
          </Form.Field>
          <Button primary>Submit</Button>
        </Form>
        <Message attached='bottom' warning size="tiny">
          <Icon name='help' />
          Already signed up? <Link to="/signin">Sign in here</Link> instead.
        </Message>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  isLoggedIn: state.auth.isLoggedIn,
  error: state.auth.error,
});

const mapDispatchToProps = dispatch => ({
  postSignup: (signupAccount) => {
    dispatch(postSignup(signupAccount));
  },
  setAuthError: (error) => {
    dispatch(setAuthError(error));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Signup);
