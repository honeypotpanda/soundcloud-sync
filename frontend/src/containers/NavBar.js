import React from 'react';
import { connect } from 'react-redux';
import { Menu } from 'semantic-ui-react';

import Login from './Login';

class NavBar extends React.Component {
  static contextTypes = {
    router: React.PropTypes.object.isRequired,
  };

  handleClick = (path) => (e) => this.context.router.push(path);

  render() {
    const { isLoggedIn } = this.props;
    return (
      <Menu>
        <Menu.Item onClick={this.handleClick('/')}>Home</Menu.Item>
        {isLoggedIn ? (
          <Menu.Menu position="right">
            <Menu.Item onClick={this.handleClick('/signout')}>Sign Out</Menu.Item>
          </Menu.Menu>
        ) : (
          <Menu.Menu position="right">
            <Login />
          </Menu.Menu>
        )}
      </Menu>
    );
  }
}

const mapStateToProps = state => ({
  isLoggedIn: state.auth.isLoggedIn,
});

export default connect(mapStateToProps)(NavBar);
