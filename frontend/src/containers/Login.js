import React from 'react';
import { connect } from 'react-redux';
import { Menu } from 'semantic-ui-react';

import { login } from '../actions/auth';

export class Login extends React.Component {
  handleClick = () => {
    FB.login(({ authResponse, status }) => {
      if (status === 'connected') {
        this.props.login();
        localStorage.setItem('accessToken', authResponse.accessToken);
        localStorage.setItem('userID', authResponse.userID);
      }
    });
  };

  render() {
    return (
      <Menu.Item onClick={this.handleClick}>Log in</Menu.Item>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  login: () => dispatch(login()),
});

export default connect(
  null,
  mapDispatchToProps
)(Login);
