import React from 'react';
import { connect } from 'react-redux';
import { Container, Header, List } from 'semantic-ui-react';

import RoomItem from '../components/RoomItem';
import { getRooms } from '../actions/room';

export class RoomList extends React.Component {
  componentDidMount() {
    this.props.getRooms();
  }

  render() {
    const { rooms } = this.props;
    return (
      <Container>
        <List>
          <List.Item>
            <List.Header as='h4'>Rooms</List.Header>
            {rooms.map(roomName => <RoomItem key={roomName} roomName={roomName} />)}
          </List.Item>
        </List>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  rooms: state.room.list,
});

const mapDispatchToProps = dispatch => ({
  getRooms: () => dispatch(getRooms()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RoomList);
