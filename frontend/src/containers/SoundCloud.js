import React from 'react';
import { connect } from 'react-redux';
import io from 'socket.io-client';
import { Button, Header, Icon, List } from 'semantic-ui-react';

import RoomButtons from '../components/RoomButtons';
import SoundCloudPlayer from '../components/SoundCloudPlayer';
import TrackList from './TrackList';

import { setTrackIndex } from '../actions/room';
import { connectSocket, disconnectSocket } from '../actions/socket';

class SoundCloud extends React.Component {
  static contextTypes = {
    router: React.PropTypes.object.isRequired,
  };

  room = '';

  constructor(props) {
    super(props);
    this.props.connectSocket();
  }

  componentDidMount() {
    this.room = this.context.router.params.roomName;
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevProps.socket && this.props.socket) {
      this.props.socket.emit('join room', { room: this.room });
    }
  }

  componentWillUnmount() {
    this.props.disconnectSocket();
  }

  render() {
    const { setTrackIndex, socket, tracks, trackIndex } = this.props;
    const track = tracks[trackIndex];
    const nextTrackIndex = (trackIndex + 1) % tracks.length;
    return (
      <div>
        <Header as="h4">SoundCloud</Header>
        {track &&
          <SoundCloudPlayer
            index={trackIndex}
            nextTrackIndex={nextTrackIndex}
            room={this.room}
            setNextTrack={(index = nextTrackIndex) => setTrackIndex(index)}
            socket={socket}
            track={track}
          />
        }
        <RoomButtons />
        <TrackList />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  socket: state.socket,
  tracks: state.track.list,
  trackIndex: state.room.trackIndex,
});

const mapDispatchToProps = dispatch => ({
  connectSocket: () => dispatch(connectSocket()),
  disconnectSocket: () => dispatch(disconnectSocket()),
  setTrackIndex: (index) => dispatch(setTrackIndex(index)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SoundCloud);
