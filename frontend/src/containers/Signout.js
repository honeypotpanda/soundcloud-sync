import React from 'react';
import { connect } from 'react-redux';

import { signOut } from '../actions/auth';

export class Signout extends React.Component {
  static contextTypes = {
    router: React.PropTypes.object.isRequired,
  };

  componentWillMount(nextProps, nextState) {
    localStorage.removeItem('soundcloud_sync_token');

    if (!this.props.isLoggedIn) {
      this.context.router.replace('/');
    } else {
      this.props.signOut();
    }
  }

  componentWillUpdate(nextProps, nextState) {
    if (!nextProps.isLoggedIn) {
      this.context.router.replace('/');
    }
  }

  render() {
    return <div>Signing out...</div>;
  }
}

const mapStateToProps = state => ({
  isLoggedIn: state.auth.isLoggedIn,
})

const mapDispatchToProps = dispatch => ({
  signOut: () => dispatch(signOut()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Signout);
