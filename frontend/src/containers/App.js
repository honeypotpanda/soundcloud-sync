import React from 'react';
import { Router, Route, browserHistory } from 'react-router';
import { connect } from 'react-redux';

import Home from '../components/Home';
import Room from '../components/Room';
import Signup from './Signup';
import Signin from './Signin';
import Signout from './Signout';

import { postAuthSuccess } from '../actions/auth';

export class App extends React.Component {
  checkAuth = (nextState, replace) => {
    const { isLoggedIn, setAuth } = this.props;
    if (!isLoggedIn && localStorage.getItem('soundcloud_sync_token')) {
      setAuth();
    }
  };

  requireNoAuth = (nextState, replace) => {
    const { isLoggedIn, setAuth } = this.props;
    if (isLoggedIn) {
      replace('/');
    } else {
      if (localStorage.getItem('soundcloud_sync_token')) {
        setAuth();
        replace('/');
      }
    }
  };

  requireAuth = (nextState, replace) => {
    const { isLoggedIn, setAuth } = this.props;
    if (!isLoggedIn) {
      if (localStorage.getItem('soundcloud_sync_token')) {
        setAuth();
      } else {
        replace('/');
      }
    }
  }

  render() {
    return (
      <Router history={browserHistory}>
        <Route path="/" component={Home} onEnter={this.checkAuth} />
        <Route path="/rooms/:roomName" component={Room} onEnter={this.checkAuth} />
        <Route path="/signup" component={Signup} onEnter={this.requireNoAuth} />
        <Route path="/signin" component={Signin} onEnter={this.requireNoAuth} />
        <Route path="/signout" component={Signout} />
      </Router>
    );
  }
}

const mapStateToProps = state => ({
  isLoggedIn: state.auth.isLoggedIn,
});

const mapDispatchToProps = dispatch => ({
  setAuth: () => dispatch(postAuthSuccess()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
