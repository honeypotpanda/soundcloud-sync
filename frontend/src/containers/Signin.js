import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import {
  Button,
  Container,
  Form,
  Icon,
  Message,
} from 'semantic-ui-react';

import { postSignin, setAuthError } from '../actions/auth';

export class Signin extends React.Component {
  static contextTypes = {
    router: React.PropTypes.object.isRequired,
  }

  state = {
    formData: {
      username: '',
      password: '',
    },
  };

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.isLoggedIn) {
      this.context.router.push('/');
    }
  }

  handleSubmit = (e, { formData }) => {
    e.preventDefault();
    const { postSignin, setAuthError } = this.props;
    postSignin(formData);
  }

  render() {
    const { error } = this.props;
    return (
      <Container text>
        <Message
          attached
          header="Welcome back to SoundCloud Sync!"
          content="Please fill out the form below to sign in"
        />
        <Form className="attached fluid segment" onSubmit={this.handleSubmit}>
          <Message
            error
            header='An error ocurred'
            content={error}
            visible={error !== ''}
          />
          <Form.Field>
            <label>Username</label>
            <input type="text" name="username" required />
          </Form.Field>
          <Form.Field>
            <label>Password</label>
            <input type="password" name="password" required />
          </Form.Field>
          <Button primary>Submit</Button>
        </Form>
        <Message attached='bottom' warning size="tiny">
          <Icon name='help' />
          Don't have an account? <Link to="/signup">Sign up here</Link> instead.
        </Message>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  isLoggedIn: state.auth.isLoggedIn,
  error: state.auth.error,
});

const mapDispatchToProps = dispatch => ({
  postSignin: (signinAccount) => {
    dispatch(postSignin(signinAccount));
  },
  setAuthError: (error) => {
    dispatch(setAuthError(error));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Signin);
