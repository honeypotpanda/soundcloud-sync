import axios from 'axios';

import {
  ADD_TRACK_REQUEST,
  ADD_TRACK_SUCCESS,
  ADD_TRACK_FAILURE,
  RESET_TRACKS,
  REMOVE_TRACK_REQUEST,
  REMOVE_TRACK_SUCCESS,
  REMOVE_TRACK_FAILURE,
} from '../action-types/track';

export const addTrackRequest = () => ({
  type: ADD_TRACK_REQUEST,
});

export const addTrackSuccess = (track) => ({
  type: ADD_TRACK_SUCCESS,
  track,
});

export const addTrackFailure = (error) => ({
  type: ADD_TRACK_FAILURE,
  error,
});

export const addTrack = (room, url) => (dispatch) => {
  dispatch(addTrackRequest());
  return axios.post('http://localhost:8300/api/tracks', { room, url })
    .then(response => dispatch(addTrackSuccess(response.data)))
    .catch(error => dispatch(addTrackFailure(error.response.data)));
};

export const resetTracks = () => ({
  type: RESET_TRACKS,
});

export const removeTrackRequest = () => ({
  type: REMOVE_TRACK_REQUEST,
});

export const removeTrackSuccess = (data) => ({
  type: REMOVE_TRACK_SUCCESS,
  data,
});

export const removeTrackFailure = (error) => ({
  type: REMOVE_TRACK_FAILURE,
  error,
});

export const removeTrack = (room, id) => (dispatch) => {
  dispatch(removeTrackRequest());
  return axios.delete(`http://localhost:8300/api/rooms/${room}/tracks/${id}`)
    .then(response => dispatch(removeTrackSuccess(response.data)))
    .catch(error => dispatch(removeTrackFailure(error.response.data)));
};

export default {
  addTrackRequest,
  addTrackSuccess,
  addTrackFailure,
  addTrack,
  resetTracks,
  removeTrackRequest,
  removeTrackSuccess,
  removeTrackFailure,
  removeTrack,
};
