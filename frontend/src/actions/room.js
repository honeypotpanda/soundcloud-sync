import axios from 'axios';

import {
  GET_ROOMS_REQUEST,
  GET_ROOMS_SUCCESS,
  GET_ROOMS_FAILURE,
  POST_ROOM_REQUEST,
  POST_ROOM_SUCCESS,
  POST_ROOM_FAILURE,
  GET_ROOM_INFO_REQUEST,
  GET_ROOM_INFO_SUCCESS,
  GET_ROOM_INFO_FAILURE,
  SET_TRACK_INDEX,
} from '../action-types/room';

export const getRoomsRequest = () => ({
  type: GET_ROOMS_REQUEST,
});

export const getRoomsSuccess = (data) => ({
  type: GET_ROOMS_SUCCESS,
  data,
});

export const getRoomsFailure = (error) => ({
  type: GET_ROOMS_FAILURE,
  error,
});

export const getRooms = () => (dispatch) => {
  dispatch(getRoomsRequest());
  return axios.get('http://localhost:8200/api/rooms')
    .then(response => dispatch(getRoomsSuccess(response.data)))
    .catch(error => dispatch(getRoomsFailure(error.response.data)));
};

export const postRoomRequest = () => ({
  type: POST_ROOM_REQUEST,
});

export const postRoomSuccess = (roomName) => ({
  type: POST_ROOM_SUCCESS,
  room: roomName,
});

export const postRoomFailure = (error) => ({
  type: POST_ROOM_FAILURE,
  error,
});

export const postRoom = (roomName) => (dispatch) => {
  dispatch(postRoomRequest());
  return axios({
    url: 'http://localhost:8200/api/rooms',
    method: 'post',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('soundcloud_sync_token')}`,
    },
    data: {
      name: roomName,
    },
  })
  .then((response) => {
    return dispatch(postRoomSuccess(roomName));
  })
  .catch((error) => {
    return dispatch(postRoomFailure(error.response.data));
  });
};

export const getRoomInfoRequest = () => ({
  type: GET_ROOM_INFO_REQUEST,
});

export const getRoomInfoSuccess = (info) => ({
  type: GET_ROOM_INFO_SUCCESS,
  info,
});

export const getRoomInfoFailure = (error) => ({
  type: GET_ROOM_INFO_FAILURE,
  error,
});

export const getRoomInfo = (room) => (dispatch) => {
  dispatch(getRoomInfoRequest());
  return axios.get(`http://localhost:8200/api/rooms/${room}`)
    .then(response => dispatch(getRoomInfoSuccess(response.data)))
    .catch(error => dispatch(getRoomInfoFailure(error.response.data)));
};

export const setTrackIndex = (index) => ({
  type: SET_TRACK_INDEX,
  index,
});

export default {
  getRoomsRequest,
  getRoomsSuccess,
  getRoomsFailure,
  getRooms,
  postRoomRequest,
  postRoomSuccess,
  postRoomFailure,
  postRoom,
  getRoomInfoRequest,
  getRoomInfoSuccess,
  getRoomInfoFailure,
  getRoomInfo,
  setTrackIndex,
};
