import axios from 'axios';

import {
  AUTHENTICATE_USER,
  LOGIN,
  POST_AUTH_REQUEST,
  POST_AUTH_SUCCESS,
  POST_AUTH_FAILURE,
  SET_AUTH_ERROR,
  SIGN_OUT,
} from '../action-types/auth';

export const authenticateUser = () => ({
  type: AUTHENTICATE_USER,
});

export const login = () => ({
  type: LOGIN,
});

export const postAuthRequest = () => ({
  type: POST_AUTH_REQUEST,
});

export const postAuthSuccess = () => ({
  type: POST_AUTH_SUCCESS,
});

export const postAuthFailure = error => ({
  type: POST_AUTH_FAILURE,
  error,
});

export const postSignup = (signupAccount) => (dispatch) => {
  let statusCode;
  dispatch(postAuthRequest());
  return axios({
    url: 'http://localhost:8100/auth/signup',
    method: 'post',
    data: signupAccount,
  })
  .then((response) => {
    localStorage.setItem('soundcloud_sync_token', response.data.soundcloud_sync_token);
    return dispatch(postAuthSuccess());
  })
  .catch((error) => {
    return dispatch(postAuthFailure(error.response.data));
  });
};

export const postSignin = (signinAccount) => (dispatch) => {
  let statusCode;
  dispatch(postAuthRequest());
  return axios({
    url: 'http://localhost:8100/auth/signin',
    method: 'post',
    data: signinAccount,
  })
  .then((response) => {
    localStorage.setItem('soundcloud_sync_token', response.data.soundcloud_sync_token);
    return dispatch(postAuthSuccess());
  })
  .catch((error) => {
    return dispatch(postAuthFailure(error.response.data));
  });
};

export const setAuthError = error => ({
  type: SET_AUTH_ERROR,
  error,
});

export const signOut = () => ({
  type: SIGN_OUT,
});

export default {
  authenticateUser,
  login,
  postAuthRequest,
  postAuthSuccess,
  postAuthFailure,
  postSignup,
  postSignin,
  setAuthError,
  signOut,
};
