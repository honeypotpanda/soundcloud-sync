import {
  CONNECT_SOCKET,
  DISCONNECT_SOCKET,
} from '../action-types/socket';

export const connectSocket = () => ({
  type: CONNECT_SOCKET,
});

export const disconnectSocket = () => ({
  type: DISCONNECT_SOCKET,
});

export default {
  connectSocket,
  disconnectSocket,
};
