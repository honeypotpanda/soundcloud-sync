import React from 'react';
import { Button, Image, List } from 'semantic-ui-react';

class Track extends React.Component {
  render() {
    const {
      artworkUrl,
      permalinkUrl,
      title,
      username,
      onRemoveClick,
      isActive,
    } = this.props;
    return (
      <List.Item>
        <Image src={artworkUrl} shape="circular" size="tiny" />
        {isActive ? (
          <List.Content>
            <List.Header>
              <a href={permalinkUrl} target="_blank">
                {title} - {username}
              </a>
            </List.Header>
          </List.Content>
        ) : (
          <List.Content>
            <a href={permalinkUrl} target="_blank">
              {title} - {username}
            </a>
          </List.Content>
        )}
        <Button
          circular
          floated="right"
          icon="trash outline"
          onClick={onRemoveClick}
        />
      </List.Item>
    );
  }
}

export default Track;
