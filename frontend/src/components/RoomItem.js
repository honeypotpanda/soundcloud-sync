import React from 'react';
import { Link } from 'react-router';
import { List } from 'semantic-ui-react';

const RoomItem = ({ roomName }) => (
  <List.Item><Link to={`/rooms/${roomName}`}>{roomName}</Link></List.Item>
);

export default RoomItem;
