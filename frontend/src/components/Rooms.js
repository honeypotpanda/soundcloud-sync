import React from 'react';
import { Grid } from 'semantic-ui-react';
import { Link } from 'react-router';

import AddRoom from '../containers/AddRoom';
import RoomList from '../containers/RoomList';

const Rooms = () => (
  <div>
    <Grid columns='equal'>
      <Grid.Column />
      <Grid.Column textAlign="center">
        <RoomList />
        <AddRoom />
      </Grid.Column>
      <Grid.Column />
    </Grid>
  </div>
);

export default Rooms;
