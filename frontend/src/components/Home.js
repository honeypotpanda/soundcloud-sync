import React from 'react';
import { Container, Header } from 'semantic-ui-react';
import { Link } from 'react-router';

import NavBar from '../containers/NavBar';
import Rooms from './Rooms';

const Home = () => (
  <div>
    <NavBar />
    <Header as="h2" textAlign="center">SoundCloud Sync</Header>
    <Container>
      <Rooms />
    </Container>
  </div>
);

export default Home;
