import * as React from 'react';
import { connect } from 'react-redux';

class SoundCloudPlayer extends React.Component {
  widget = null;
  isPlaying = false;
  receivedPauseEvent = false;
  receivedPlayEvent = false;
  seeked = false;
  playTime = 0;

  initializeWidget = () => {
    const {
      index,
      nextTrackIndex,
      room,
      setNextTrack,
      socket,
    } = this.props;
    this.widget = SC.Widget(document.querySelector('iframe'));

    this.widget.bind(SC.Widget.Events.FINISH, () => {
      setNextTrack();
      socket.emit('finish', { nextTrackIndex, room });
    });

    this.widget.bind(SC.Widget.Events.PAUSE, () => {
      if (!this.receivedPauseEvent) {
        socket.emit('pause', { room });
      } else {
        this.receivedPauseEvent = false;
      }
      this.isPlaying = false;
    });

    this.widget.bind(SC.Widget.Events.PLAY, () => {
      if (!this.receivedPlayEvent) {
        this.widget.getPosition(time => {
            socket.emit('play', { time, room });
        });
      } else {
        this.seeked = true;
        this.widget.seekTo(this.playTime);
        this.receivedPlayEvent = false;
        this.isPlaying = true;
      }
    });

    this.widget.bind(SC.Widget.Events.SEEK, () => {
      if (this.seeked) {
        this.seeked = false;
      } else {
        this.widget.getPosition(time => {
            socket.emit('seek', { time, room });
        });
      }
    });
  }

  componentDidMount() {
    const { setNextTrack, socket } = this.props;

    this.initializeWidget();

    socket.on('pause', () => {
      this.receivedPauseEvent = true;
      this.widget.pause();
    });

    socket.on('play', ({ time }) => {
      this.receivedPlayEvent = true;
      this.playTime = time;
      this.widget.play();
    });

    socket.on('seek', ({ time }) => {
      this.seeked = true;
      this.widget.seekTo(time);
    });

    socket.on('set next track', ({ nextTrackIndex }) => {
      setNextTrack(nextTrackIndex);
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.index !== this.props.index) {
      this.initializeWidget();
    }
  }

  render() {
    const { track } = this.props;
    return (
      <div
          dangerouslySetInnerHTML={{ __html: track.html }}
      />
    );
  }
}

export default SoundCloudPlayer;
