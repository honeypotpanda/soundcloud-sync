import React from 'react';
import { Container } from 'semantic-ui-react';

import NavBar from '../containers/NavBar';
import SoundCloud from '../containers/SoundCloud';

const Room = () => (
  <div>
    <NavBar />
    <Container>
      <SoundCloud />
    </Container>
  </div>
);

export default Room;
