import React from 'react';
import { Button, Icon } from 'semantic-ui-react';

import AddTrack from '../containers/AddTrack';

class RoomButtons extends React.Component {
  render() {
    return (
      <div>
        <Button.Group>
          <AddTrack />
        </Button.Group>
        {' '}
        <Button.Group icon>
          <Button>
            <Icon name='backward' />
          </Button>
          <Button>
            <Icon name='play' />
          </Button>
          <Button>
            <Icon name='forward' />
          </Button>
        </Button.Group>
      </div>
    );
  }
}

export default RoomButtons;
