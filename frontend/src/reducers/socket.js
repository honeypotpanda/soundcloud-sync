import io from 'socket.io-client';

import {
  CONNECT_SOCKET,
  DISCONNECT_SOCKET,
} from '../action-types/socket';

const initialState = null;

function socket(state = initialState, action) {
  switch (action.type) {
    case CONNECT_SOCKET:
      return io.connect('http://localhost:8300');
    case DISCONNECT_SOCKET:
      state.disconnect();
      return null;
    default:
      return state;
  }
}

export default socket;
