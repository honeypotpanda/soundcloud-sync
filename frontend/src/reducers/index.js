import { combineReducers } from 'redux';

import auth from './auth';
import room from './room';
import socket from './socket';
import track from './track';

export default combineReducers({
    auth,
    room,
    socket,
    track,
});
