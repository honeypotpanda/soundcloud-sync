import {
  GET_ROOMS_REQUEST,
  GET_ROOMS_SUCCESS,
  GET_ROOMS_FAILURE,
  POST_ROOM_REQUEST,
  POST_ROOM_SUCCESS,
  POST_ROOM_FAILURE,
  GET_ROOM_INFO_REQUEST,
  GET_ROOM_INFO_SUCCESS,
  GET_ROOM_INFO_FAILURE,
  SET_TRACK_INDEX,
} from '../action-types/room';

const initialState = {
  isFetching: false,
  list: [],
  error: '',
  trackIndex: 0,
};

function room(state = initialState, action) {
  switch (action.type) {
    case GET_ROOMS_REQUEST:
    case POST_ROOM_REQUEST:
    case GET_ROOM_INFO_REQUEST:
      return Object.assign({}, state, { isFetching: true, error: '' });
    case GET_ROOMS_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        list: action.data.rooms.map(room => room.name),
      });
    case POST_ROOM_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        list: [
          ...state.list,
          action.room
        ]
      });
    case GET_ROOMS_FAILURE:
    case POST_ROOM_FAILURE:
    case GET_ROOM_INFO_FAILURE:
      return Object.assign({}, state, { isFetching: false, error: action.error.message });
    case GET_ROOM_INFO_SUCCESS:
      return {
        ...state,
        isFetching: false,
        trackIndex: action.info.trackIndex,
      };
    case SET_TRACK_INDEX:
      return {
        ...state,
        trackIndex: action.index,
      };
    default:
      return state;
  }
}

export default room;
