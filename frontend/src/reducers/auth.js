import {
  AUTHENTICATE_USER,
  LOGIN,
  POST_AUTH_REQUEST,
  POST_AUTH_SUCCESS,
  POST_AUTH_FAILURE,
  SET_AUTH_ERROR,
  SIGN_OUT,
} from '../action-types/auth';

const initialState = {
  isFetching: false,
  isLoggedIn: false,
  error: '',
};

function auth(state = initialState, action) {
  switch (action.type) {
    case AUTHENTICATE_USER:
    case LOGIN:
      return {
        ...state,
        isLoggedIn: true,
      };
    case POST_AUTH_REQUEST:
      return Object.assign({}, state, { isFetching: true, error: '' });
    case POST_AUTH_SUCCESS:
      return Object.assign({}, state, { isFetching: false, isLoggedIn: true });
    case POST_AUTH_FAILURE:
      return Object.assign({}, state, { isFetching: false, error: action.error.message });
    case SET_AUTH_ERROR:
      return Object.assign({}, state, { error: action.error.message });
    case SIGN_OUT:
      return initialState;
    default:
      return state;
  }
}

export default auth;
