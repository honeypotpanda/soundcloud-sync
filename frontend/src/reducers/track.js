import {
  ADD_TRACK_REQUEST,
  ADD_TRACK_SUCCESS,
  ADD_TRACK_FAILURE,
  RESET_TRACKS,
  REMOVE_TRACK_REQUEST,
  REMOVE_TRACK_SUCCESS,
  REMOVE_TRACK_FAILURE,
} from '../action-types/track';
import { GET_ROOM_INFO_SUCCESS } from '../action-types/room';

const initialState = {
  isFetching: false,
  list: [],
  error: '',
};

function track(state = initialState, action) {
  switch (action.type) {
    case ADD_TRACK_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: '',
      };
    case ADD_TRACK_SUCCESS:
      return {
        ...state,
        isFetching: false,
        list: [
          ...state.list,
          {
            id: action.track.id,
            html: action.track.html,
            title: action.track.title,
            username: action.track.username,
            artwork_url: action.track.artwork_url,
            permalink_url: action.track.permalink_url,
          }
        ],
      };
    case ADD_TRACK_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: action.error.message,
      };
    case RESET_TRACKS:
      return Object.assign({}, state, { list: [] });
    case REMOVE_TRACK_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: '',
      };
    case REMOVE_TRACK_SUCCESS:
      return {
        ...state,
        isFetching: false,
        list: [
          ...state.list.slice(0, action.data.index),
          ...state.list.slice(action.data.index + 1),
        ],
      }
    case REMOVE_TRACK_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: action.error.message,
      };
    case GET_ROOM_INFO_SUCCESS:
      return {
        ...state,
        list: action.info.tracks,
      };
    default:
      return state;
  }
}

export default track;
