export const GET_ROOMS_REQUEST = 'GET_ROOMS_REQUEST';
export const GET_ROOMS_SUCCESS = 'GET_ROOMS_SUCCESS';
export const GET_ROOMS_FAILURE = 'GET_ROOMS_FAILURE';
export const POST_ROOM_REQUEST = 'POST_ROOM_REQUEST';
export const POST_ROOM_SUCCESS = 'POST_ROOM_SUCCESS';
export const POST_ROOM_FAILURE = 'POST_ROOM_FAILURE';
export const GET_ROOM_INFO_REQUEST = 'GET_ROOM_INFO_REQUEST';
export const GET_ROOM_INFO_SUCCESS = 'GET_ROOM_INFO_SUCCESS';
export const GET_ROOM_INFO_FAILURE = 'GET_ROOM_INFO_FAILURE';
export const SET_TRACK_INDEX = 'SET_TRACK_INDEX';

export default {
  GET_ROOMS_REQUEST,
  GET_ROOMS_SUCCESS,
  GET_ROOMS_FAILURE,
  POST_ROOM_REQUEST,
  POST_ROOM_SUCCESS,
  POST_ROOM_FAILURE,
  GET_ROOM_INFO_REQUEST,
  GET_ROOM_INFO_SUCCESS,
  GET_ROOM_INFO_FAILURE,
  SET_TRACK_INDEX,
};
